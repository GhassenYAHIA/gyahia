SELECT film.id_genre , genre.nom AS 'nom genre', film.id_distrib AS 'id distrib', distrib.nom AS 'nom distrib', titre AS'titre film' FROM film
RIGHT JOIN distrib ON film.id_distrib=distrib.id_distrib 
RIGHT JOIN genre ON film.id_genre=genre.id_genre
WHERE film.id_genre BETWEEN 4 and 8;
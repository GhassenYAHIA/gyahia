CREATE TABLE `genre` (
  `id_genre` tinyint(4) NOT NULL,
  `nom` varchar(255) NOT NULL,
  PRIMARY KEY (`id_genre`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `genre` VALUES 
(0,'detective'),
(1,'dramatic comedy'),
(2,'science fiction'),
(3,'drama'),
(4,'documentary'),
(5,'animation'),
(6,'comedy'),
(7,'fantasy'),
(8,'action'),
(9,'thriller'),
(10,'adventure'),
(11,'various'),
(12,'historical'),
(13,'western'),
(14,'romance'),
(15,'music'),
(16,'musical'),
(17,'horror'),
(18,'war'),
(19,'unknow'),
(20,'spying'),
(21,'historical epic'),
(22,'biography'),
(23,'experimental'),
(24,'short film'),
(25,'erotic'),
(26,'karate'),
(27,'program'),
(28,'family'),
(29,'exp&amp;atilde;&amp;copy;rimental');

SELECT film.titre AS 'Titre',film.resum AS 'Resume', film.annee_prod FROM film
RIGHT JOIN genre ON film.id_genre=genre.id_genre WHERE genre.nom='erotic'
ORDER BY film.annee_prod DESC;


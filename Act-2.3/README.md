CONTENU DU DOSSIER ACT-2.3:

1/ Création d'une base de donnée

2/ Création d'une table

3/ Insertion de valeurs dans une table

4/ Copie de valeurs d'une table à une autre sopus conditions

5/ MAJ de la valeur de la date pour les nouvelles entrées dans la table

6/ Supprimer les 5 premiers membres de la table 

7/ Affichage de titre et résumé avec condition sur résumé. Recherche insensibleà la casse. Affichage ordonné.

8/ Affichage titre et résumé avec conditions.

9/ Affichage partie de chaine de caractères.

10/ Compter un nombre et l'afficher.

11/ Affichage par genre.

12/ Affichage avec jointure multiple.

13/ affichage des noms et prenoms composés.

14/ Moyenne du nombre de sièges.

15/ Affichage ordonné des nombres de sièges par étage.

16/ Selection de numéro de téléphone débutant par 05, inversion des numéros et suppression du 0.

17/ Affichage du nombre de films dans un intervalle de dates précises.

18/ Calcul de somme, moyennes, modulo.

19/ Affichage pour certaines valeurs, à partir d'un certain rang et pour un nombre précis de résultats.

20/ nombre de jours absolus.

21/ Affichage des abonnés et leurs dates d'abonnement.

22/ Pareil que précédent avec nom non null.

23/ Addition d'une entrée à la table film.

24/ Affichage restreint d'une liste de films pour certaines conditions.

25/ Cryptage d'un numéro de téléphone.

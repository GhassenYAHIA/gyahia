package com.vintud.utils;

import java.sql.Connection;
import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

public class ConnectionManager {

	private static EntityManager instance;

	private ConnectionManager() {

	}

	private static String url = "jdbc:mysql://localhost:3306/vintud?serverTimezone=UTC";

	private static String username = "root";

	private static String password = "";

	// objet Connection

	private static EntityManager connect;

	// methode qui va retourner notre instance et la crée si elle n'existe pas
	public static EntityManager getInstance() throws ClassNotFoundException, SQLException {
		
		if (connect == null) {
			
			EntityManagerFactory emf=Persistence.createEntityManagerFactory("persistence");
			EntityManager em=emf.createEntityManager();
			connect = em;

		}

		return connect;
	}



}
package com.vintud.entity;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int idUser;
	@Column(name = "firstname")
	private String firstName;
	@Column(name = "name")
	private String lastName;
	@Column(name = "pseudo")
	private String pseudo;
	@Column(name = "u_password")
	private String password;
	@Column(name = "mail")
	private String mail;
	@Column(name = "phone")
	private int phone;
	@Column(name = "address")
	private String address;
	//@ManyToOne
	@Column(name = "role_id")
	private int roleId;
	@Transient
	private ArrayList<Ad> postedAd;
	@Transient
	private ArrayList<Ad> favoriteAd;

	public User() {
	}

	public User(String firstName, String lastName, String pseudo,  String mail,String password, int phone,
			String address, int roleId) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.pseudo = pseudo;
		this.password = password;
		this.mail = mail;
		this.phone = phone;
		this.address = address;
		this.roleId = roleId;
	}

	// getters
	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPseudo() {
		return pseudo;
	}

	public String getPassword() {
		return password;
	}

	public String getMail() {
		return mail;
	}

	public int getPhone() {
		return phone;
	}

	public String getAddress() {
		return address;
	}

	public ArrayList<Ad> getPostedAd() {
		return postedAd;
	}

	public ArrayList<Ad> getFavoriteAd() {
		return favoriteAd;
	}

	public int getRoleId() {
		return roleId;
	}

	public int getIdUser() {
		return idUser;
	}

	// setters

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setPostedAd(ArrayList<Ad> postedAd) {
		this.postedAd = postedAd;
	}

	public void setFavoriteAd(ArrayList<Ad> favoriteAd) {
		this.favoriteAd = favoriteAd;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName + ", pseudo=" + pseudo + ", mail=" + mail
				+ ", phone=" + phone + ", address=" + address + ", roleId=" + roleId + ", idUser=" + idUser + "]";
	}

	// comparing users on the basis of having same, first and last name, address and
	// phone number
	public boolean equals(User obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (favoriteAd == null) {
			if (other.favoriteAd != null)
				return false;
		} else if (!favoriteAd.equals(other.favoriteAd))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (idUser != other.idUser)
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (mail == null) {
			if (other.mail != null)
				return false;
		} else if (!mail.equals(other.mail))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (phone != other.phone)
			return false;
		if (postedAd == null) {
			if (other.postedAd != null)
				return false;
		} else if (!postedAd.equals(other.postedAd))
			return false;
		if (pseudo == null) {
			if (other.pseudo != null)
				return false;
		} else if (!pseudo.equals(other.pseudo))
			return false;
		if (roleId != other.roleId)
			return false;
		return true;
	}

}

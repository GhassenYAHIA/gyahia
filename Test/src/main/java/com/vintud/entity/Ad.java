package com.vintud.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

enum Status {

	waitingValidation, validated, deleted;
}

@Entity
@Table(name = "announcement")
public class Ad {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int idAd;
	@Column(name = "title")
	private String titleAd;
	@Column(name = "description")
	private String descriptionAd;
	/*
	 * @OneToMany
	 * 
	 * @JoinColumn(name="category_id")
	 */
	// @Column(name="category_id")
	@OneToOne
	private Category categoryAd;
	@Column(name = "price")
	private double priceAd;
	@Column(name = "picture")
	private String pictureAd;
	@Column(name = "publicaton_date")
	private Timestamp datePublication;
	@Column(name = "is_available")
	private boolean availabilityAd;
	@Column(name = "view_number")
	private int numberView;
	@Column(name = "localisation")
	private String localisationAd;
	@OneToOne
	@JoinColumn(name = "user_id")
	private User userAd;

	public Ad() {

	}

	public Ad(String titleAd, String pictureAd, String descriptionAd, double priceAd, Timestamp datePublication,
			boolean availabilityAd, String localisationAd, int numberView, User userAd, Category categoryAd) {

		this.titleAd = titleAd;
		this.pictureAd = pictureAd;
		this.descriptionAd = descriptionAd;
		this.priceAd = priceAd;
		this.datePublication = datePublication;
		this.availabilityAd = availabilityAd;
		this.localisationAd = localisationAd;
		this.numberView = numberView;
		this.userAd = userAd;
		this.categoryAd = categoryAd;
	}

	@Override
	public String toString() {
		return "Ad [idAd=" + idAd + ", titleAd=" + titleAd + ", pictureAd=" + pictureAd + ", descriptionAd="
				+ descriptionAd + ", priceAd=" + priceAd + ", datePublication=" + datePublication + ", availabilityAd="
				+ availabilityAd + ", localisationAd=" + localisationAd + ", numberView=" + numberView + "]";
	}

	public boolean equals(Ad obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ad other = (Ad) obj;
		if (availabilityAd != other.availabilityAd)
			return false;
		if (datePublication == null) {
			if (other.datePublication != null)
				return false;
		} else if (!datePublication.equals(other.datePublication))
			return false;
		if (descriptionAd == null) {
			if (other.descriptionAd != null)
				return false;
		} else if (!descriptionAd.equals(other.descriptionAd))
			return false;
		if (idAd != other.idAd)
			return false;
		if (localisationAd == null) {
			if (other.localisationAd != null)
				return false;
		} else if (!localisationAd.equals(other.localisationAd))
			return false;
		if (numberView != other.numberView)
			return false;
		if (pictureAd == null) {
			if (other.pictureAd != null)
				return false;
		} else if (!pictureAd.equals(other.pictureAd))
			return false;
		if (Double.doubleToLongBits(priceAd) != Double.doubleToLongBits(other.priceAd))
			return false;
		if (titleAd == null) {
			if (other.titleAd != null)
				return false;
		} else if (!titleAd.equals(other.titleAd))
			return false;
		return true;
	}

// getter
	public int getIdAd() {
		return idAd;
	}

	public String getTitleAd() {
		return titleAd;
	}

	public String getPictureAd() {
		return pictureAd;
	}

	public String getDescriptionAd() {
		return descriptionAd;
	}

	public double getPriceAd() {
		return priceAd;
	}

	public Timestamp getDatePublication() {
		return datePublication;
	}

	public boolean isAvailabilityAd() {
		return availabilityAd;
	}

	public String getLocalisationAd() {
		return localisationAd;
	}

	public int getNumberView() {
		return numberView;
	}

	public User getUserAd() {
		return userAd;
	}

	public Category getCategoryAd() {
		return categoryAd;
	}

	// setter
	public void setIdAd(int idAd) {
		this.idAd = idAd;
	}

	public void setTitleAd(String titleAd) {
		this.titleAd = titleAd;
	}

	public void setPictureAd(String pictureAd) {
		this.pictureAd = pictureAd;
	}

	public void setDescriptionAd(String descriptionAd) {
		this.descriptionAd = descriptionAd;
	}

	public void setPriceAd(double priceAd) {
		this.priceAd = priceAd;
	}

	public void setDatePublication(Timestamp datePublication) {
		this.datePublication = datePublication;
	}

	public void setAvailabilityAd(boolean availabilityAd) {
		this.availabilityAd = availabilityAd;
	}

	public void setLocalisationAd(String localisationAd) {
		this.localisationAd = localisationAd;
	}

	public void setNumberView(int numberView) {
		this.numberView = numberView;
	}

	public void setUserAd(User userAd) {
		this.userAd = userAd;
	}

	public void setCategoryAd(Category categoryAd) {
		this.categoryAd = categoryAd;
	}

}

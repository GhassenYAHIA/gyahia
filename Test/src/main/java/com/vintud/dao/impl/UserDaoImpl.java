package com.vintud.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.vintud.dao.IUserDao;
import com.vintud.dao.factory.FactoryDao;
import com.vintud.entity.Roles;
import com.vintud.entity.User;

@Repository
public class UserDaoImpl implements IUserDao {

	private FactoryDao factoryDao;

	public UserDaoImpl(FactoryDao factoryDao) {
		this.factoryDao = factoryDao;
	}

	// méthode affichant les nombres de vues d'une annonce

	public String getViews(int idAd) throws SQLException, ClassNotFoundException {
		EntityManager connect = FactoryDao.getConnection();
		Statement stm = null;
		ResultSet rs = null;

		String query = "SELECT  id, title, view_number FROM announcement WHERE id=" + idAd + ";";
		// stm = connect.createStatement();
		rs = stm.executeQuery(query);
		String results = "";
		while (rs.next())
			results = rs.getString(1) + " - " + rs.getString(2) + " - " + rs.getString(3);

		return results;
	}

	// méthode créant un nouvel utilisateur
	public int create(User user) throws SQLException, ClassNotFoundException {
		EntityManager connect = FactoryDao.getConnection();

		connect.getTransaction().begin();
		User result = connect.merge(user);
		connect.getTransaction().commit();

		return result.getIdUser();

	}

	// méthode permettant le login d'un utilisateur

	public String login(String mail, String password) throws SQLException, ClassNotFoundException {
		EntityManager connect = FactoryDao.getConnection();

		connect.getTransaction().begin();
		Query query = connect.createQuery("SELECT U.id FROM  User U WHERE U.mail=:usermail AND U.password=:password");
		query.setParameter("usermail", mail);
		query.setParameter("password", password);
		connect.getTransaction().commit();
		String result = "";
		if (!query.getResultList().isEmpty()) {
			result = "authorized";
		}
		System.out.println(result);
		return result;
	}

	// méthode pour modifier des informations personnelles d'un utilisateur
	public int update(User user, String pseudo, String mail, int phone, String address)
			throws SQLException, ClassNotFoundException {

		EntityManager connect = FactoryDao.getConnection();

		System.out.println("Updating entry for user:\nPseudo, Mail, Phone and Address.\n");
		user.setPseudo(pseudo);
		user.setMail(mail);
		user.setPhone(phone);
		user.setAddress(address);
		String firstname = user.getFirstName();
		String lastname = user.getLastName();

		connect.getTransaction().begin();
		Query query = connect.createQuery(
				"update User u  set u.pseudo=:pseudo, u.mail=:mail,u.phone=:phone,u.address=:address where u.firstName=:firstname and u.lastName=:lastname");
		query.setParameter("firstname", firstname);
		query.setParameter("lastname", lastname);
		query.setParameter("pseudo", pseudo);
		query.setParameter("mail", mail);
		query.setParameter("address", address);
		query.setParameter("phone", phone);
		int result = query.executeUpdate();
		connect.getTransaction().commit();

		return result;
	}

	// méthode pour retrouver les informations d'un vendeur
	public List getBuyerInformation(User user) throws SQLException, ClassNotFoundException {
		EntityManager connect = FactoryDao.getConnection();

		Query query = connect
				.createQuery("SELECT u.id FROM User u where u.firstName =:firstname AND u.lastName =:lastname");
		query.setParameter("firstname", user.getFirstName());
		query.setParameter("lastname", user.getLastName());
		List result = null;

		// verification of the existence of the user in the DB
		if (!query.getResultList().isEmpty()) {
			int id = (int) query.getSingleResult();
			user.setIdUser(id);
			query = connect
					.createQuery("SELECT u.id,u.firstName,u.lastName,u.mail,u.phone,a.idAd,a.titleAd FROM User u "
							+ "RIGHT JOIN Ad a ON  u.id=a.userAd WHERE u.id=" + user.getIdUser());

			// verification if the user is a buyer
			if (!query.getResultList().isEmpty()) {
				result = query.getResultList();
			}

		}
		return result;
	}

	// méthode pour modifier le rôle d'un utilisateur

	public int updateRole(User user, Roles roleUser) throws SQLException, ClassNotFoundException {
		EntityManager connect = FactoryDao.getConnection();

		user.setRoleId(roleUser.ordinal());
		Query query = connect
				.createQuery("select u.id from User u where u.firstName=:firstname and u.lastName=:lastname");
		query.setParameter("firstname", user.getFirstName());
		query.setParameter("lastname", user.getLastName());
		List result = null;

		// verification of the existence of the user in the DB
		if (!query.getResultList().isEmpty()) {
			int id = (int) query.getSingleResult();
			user.setIdUser(id);

			int role = user.getRoleId() + 1;
			query = connect.createQuery("Update User u SET u.roleId=" + role + " WHERE u.id=" + user.getIdUser());
			connect.getTransaction().begin();
			query.executeUpdate();
			connect.getTransaction().commit();
		}
		// System.out.println(user.getIdUser());
		return user.getIdUser();

	}

	public void delete(int i) throws SQLException {

	}

}

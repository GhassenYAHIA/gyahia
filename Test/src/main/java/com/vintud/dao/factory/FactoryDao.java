package com.vintud.dao.factory;

import java.sql.SQLException;

import javax.persistence.EntityManager;

import com.vintud.dao.IUserDao;
import com.vintud.dao.impl.UserDaoImpl;
import com.vintud.utils.ConnectionManager;

public class FactoryDao {

	// retourne un objet interagissant avec la base de donn�e
	public IUserDao getUserDao() {
		return new UserDaoImpl(this);
	}

	public static EntityManager getConnection() throws ClassNotFoundException, SQLException {
		return ConnectionManager.getInstance();
	}

}

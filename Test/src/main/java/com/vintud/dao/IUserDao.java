package com.vintud.dao;

import java.sql.SQLException;
import java.util.List;

import com.vintud.entity.Roles;
import com.vintud.entity.User;

public interface IUserDao{

	public String getViews(int idAd) throws SQLException, ClassNotFoundException;


	public int create(User user) throws SQLException, ClassNotFoundException;

	public int update(User user,String pseudo, String mail, int phone, String address) throws SQLException, ClassNotFoundException;
	
	public List getBuyerInformation(User user) throws SQLException, ClassNotFoundException;
	

	
	public int updateRole(User user, Roles role) throws SQLException, ClassNotFoundException;
	
	
	
	public String login(String mail, String password) throws SQLException, ClassNotFoundException;
	
}

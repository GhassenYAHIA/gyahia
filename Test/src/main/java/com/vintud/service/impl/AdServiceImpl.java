package com.vintud.service.impl;

import java.sql.SQLException;
import java.util.List;

import com.vintud.dao.factory.FactoryDao;
import com.vintud.entity.Ad;
import com.vintud.entity.User;
import com.vintud.service.IAdService;

public class AdServiceImpl implements IAdService {

	private FactoryDao dao = new FactoryDao();

	public void getAvailableAds() {
	}

	@Override
	public void filterAdsByPrice(double min, double max) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Ad announce) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(int idTodelete) {
		// TODO Auto-generated method stub

	}

	@Override
	public void getAdsByCategory(String category) {
		// TODO Auto-generated method stub

	}

	@Override
	public void filterAdsLocation(String location) {
		// TODO Auto-generated method stub

	}

	@Override
	public void create(Ad newAd, int userID, int categoryId) {
		// TODO Auto-generated method stub

	}

	public void getBuyerInfo(User user) throws ClassNotFoundException {
		List result = null;
		try {
			result = dao.getUserDao().getBuyerInformation(user);
			if (result != null) {
				System.out.println("This is the buyer you are looking for:\n");
				for (int i = 0; i < result.size(); i++) {
					System.out.println(result.get(i).toString());
				}
			} else {
				System.out.println("No such buyer in our database.");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}

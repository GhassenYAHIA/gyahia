package com.vintud.service.impl;

import java.sql.SQLException;

import com.vintud.dao.factory.FactoryDao;
import com.vintud.entity.Roles;
import com.vintud.entity.User;
import com.vintud.service.IUserService;

public class UserServiceImpl implements IUserService {
	private FactoryDao dao = new FactoryDao();

	public void createUser(User user) throws ClassNotFoundException {
		int count = 0;
		try {
			count = dao.getUserDao().create(user);
		} catch (SQLException e) {

			e.getMessage();
		}
		if (count != 0) {
			System.out.println("Signing In done.");

		} else {
			System.out.println("No new user created.\n");
		}
	}

	@Override
	public void loginUser(String mail, String password) throws ClassNotFoundException {
		String testLog = "";
		try

		{
			testLog = dao.getUserDao().login(mail, password);
			if (testLog == "authorized") {
				System.out.println("Login succeeded.");
			} else {
				System.out.println("Verify your information. Login Error.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateUser(User user, String pseudo, String mail, int phone, String address)
			throws ClassNotFoundException {
		int count = 0;
		try {
			count = dao.getUserDao().update(user, pseudo, mail, phone, address);
			System.out.println(dao.getUserDao().update(user, pseudo, mail, phone, address));
			if (count != 0) {
				System.out.println("Update on user done.");
			} else {
				System.out.println("Update not taken into count.");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateRoleUser(User user, Roles role) {
		int count = 0;
		try {
			count = dao.getUserDao().updateRole(user, role);
			if (count != 0) {
				System.out.println("User role up to date. He/She is now :" + role.name() + ".");
			} else {
				System.out.println("Update error. Check your informations.");
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}

package com.vintud.service;

import com.vintud.entity.Roles;
import com.vintud.entity.User;

public interface IUserService {

	


	void createUser(User user) throws ClassNotFoundException;
	
	void loginUser(String mail, String password) throws ClassNotFoundException;
	
	void updateUser(User user,String pseudo, String mail, int phone, String address) throws ClassNotFoundException;
	
	void updateRoleUser(User user,Roles role);

	
	
	
}
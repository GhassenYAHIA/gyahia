package com.vintud.service;


import com.vintud.entity.Ad;
import com.vintud.entity.User;


public interface IAdService {
    
    	public void getAvailableAds();
    	
	public void filterAdsByPrice(double min, double max) ;  

	public void update(Ad announce);

	public void delete(int idTodelete) ;
	
	public void getAdsByCategory(String category) ;

	public void filterAdsLocation(String location) ;

	void create(Ad newAd, int userID, int categoryId);
	void getBuyerInfo(User user) throws ClassNotFoundException;
}

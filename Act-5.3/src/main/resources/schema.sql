
CREATE database vintud character set UTF8mb4 collate utf8mb4_bin;

CREATE TABLE vintud.role(
id int NOT NULL AUTO_INCREMENT ,
nom varchar(255) NOT NULL,
PRIMARY KEY(id)
)ENGINE=InnoDB;

INSERT INTO vintud.role VALUES (1, "CLIENT");

CREATE TABLE vintud.user(
id int NOT NULL AUTO_INCREMENT ,
firstname varchar(255) NOT NULL,
name varchar(255) NOT NULL,
pseudo varchar(255) NOT NULL,
mail varchar(255) NOT NULL,
u_password varchar(255) NOT NULL,
phone int NOT NULL,
address varchar(255) NOT NULL,
role_id int NOT NULL,
PRIMARY KEY(id),
CONSTRAINT FK_RoleUser FOREIGN KEY (role_id)
    REFERENCES vintud.role(id)
)ENGINE=InnoDB;

INSERT INTO vintud.user VALUES (1, "Pirate", "Caraïbes", "Johny", "johnyPirate@Cara.com", "unmotdepassedepirate", "1563214560", "Entre l'atlantic et le pacific.",1);
INSERT INTO vintud.user VALUES (2, "Corsaire", "THP", "George", "george@Cara.com", "unmotdepassedecorsaire", "1563214561", "Entre la manche et la mer du nord.",1);
INSERT INTO vintud.user VALUES (3, "Moussaillon", "Java", "Coding", "coding@Cara.com", "unmotdepassedemoussaillon", "1563214562", "Sur la méditérranée.",1);



CREATE TABLE vintud.category(
id int NOT NULL AUTO_INCREMENT ,
name varchar(255) NOT NULL,
description VARCHAR(255) NOT NULL,
PRIMARY KEY(id)
)ENGINE=InnoDB;

INSERT INTO vintud.category VALUES (1, "T-SHIRT", "Vêtement léger souvent mis l'été.");
INSERT INTO vintud.category VALUES (2, "Pantalon", "Vêtement du bas du corp mis pour couvrir ses jambres.");
INSERT INTO vintud.category VALUES (3, "Veste", "Vêtement chaud.");


CREATE TABLE vintud.announcement(
id int NOT NULL AUTO_INCREMENT ,
title varchar(255) NOT NULL,
description VARCHAR(255) NOT NULL,
category_id int NOT NULL,
price double NOT NULL,
picture varchar(255) NULL,
publication_date DATETIME NOT NULL,
is_available boolean NOT NULL,
view_number int NOT NULL,
localisation varchar(255),String
user_id int NOT NULL,
PRIMARY KEY(id),
CONSTRAINT FK_UserAnnouncement FOREIGN KEY (user_id)
    REFERENCES vintud.user(id),
CONSTRAINT FK_CategoryAnnouncement FOREIGN KEY (category_id)
    REFERENCES vintud.category(id)
)ENGINE=InnoDB;





INSERT INTO vintud.announcement VALUES (1, "t-shirt lacuste bon état", "Je vends mon magnifique t-shirt lacuste acheté l'année dernière en solde, taille M.",1,24.98,NULL,"2019-01-25 8:32:12", true, 2,"Paris 9ième",1);
INSERT INTO vintud.announcement VALUES (2, "Veste Guoss comme nueve", "Je vends ma sublime veste Guoss achetée cette année en solde, taille S.",3,87.25,NULL,"2019-02-25 11:32:12", true, 8,"Paris 9ième",2);







CREATE TABLE vintud.favorite 
(
id int NOT NULL AUTO_INCREMENT ,
favorite_date DATETIME NOT NULL,
ad_id int NOT NULL,
user_id int NOT NULL,
PRIMARY KEY(id),
CONSTRAINT FK_UserFavortite FOREIGN KEY (user_id)
    REFERENCES vintud.user(id),
CONSTRAINT FK_AdFavorite FOREIGN KEY (ad_id)
    REFERENCES vintud.announcement(id)
)ENGINE=InnoDB;


CREATE TABLE vintud.search
(
id int NOT NULL AUTO_INCREMENT,
type varchar(255) NOT NULL,
size varchar(255) NOT NULL,
color varchar(255) NOT NULL,
min_price int NOT NULL,
max_price int NOT NULL,
PRIMARY KEY(id)
)ENGINE=InnoDB;

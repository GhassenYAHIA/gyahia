package com.thp.project.vintud.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thp.project.vintud.entity.Role;
import com.thp.project.vintud.entity.User;
import com.thp.project.vintud.repository.UserRepository;
import com.thp.project.vintud.service.UserService;

@Service

public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository repo;

	public void createUser(User user) {
		System.out.println("Creating new user with pseudo:\n" + user.getPseudo());
		repo.save(user);
		System.out.println("New user creation complete.");
	}

	public boolean login(String mail, String password) {

		boolean msg = false;
		List<User> result = repo.findByMailAndPassword(mail, password);
		

		if (!result.isEmpty()) {
			msg = true;
		}
		System.out.println("Trying to log in.");
		if (msg) {
			System.out.println("Loggin sueccesfull!");
		} else {
			System.out.println("Log in failed. Verify your informations.");
		}
		return msg;
	}

	public void updateUser(User user, String mail, String pseudo) {
		System.out.println("Updating pseudo and mail for user:\n" + user.getFirstName() + " " + user.getLastName());
		user.setPseudo(pseudo);
		user.setMail(mail);
		repo.save(user);
		System.out.println("Update done.");
	}

	@Override
	public void updateRole(User user, Role role) {
		user.setRole(role);
		repo.save(user);
		
	}
	
	

}

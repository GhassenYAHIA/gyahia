package com.thp.project.vintud.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thp.project.vintud.entity.Ad;
import com.thp.project.vintud.entity.User;
import com.thp.project.vintud.repository.AdRepository;
import com.thp.project.vintud.repository.UserRepository;
import com.thp.project.vintud.service.AdService;

@Service
public class AdServiceImpl implements AdService {

	@Autowired
	private AdRepository repo;
	@Autowired
	private UserRepository userRepo;
	
	public void createAd(Ad ad) {

		repo.save(ad);

	}

	public void viewAds() {
		List<Ad> list = repo.findAll();
		if (!list.isEmpty()) {
			for (Ad ad : list) {
				System.out.println(ad);
			}
		} else {
			System.out.println("No available ads at the moment.");
		}

	}
	public void getAdsByUser(User user) {
		Optional<User> u= Optional.ofNullable(user);
		List<Ad> list=repo.findByUserAd(u);
		if (!list.isEmpty()) {
			for (Ad ad : list) {
				System.out.println("\nThis is the list of ads by user with ID:"+user.getIdUser()+"\n");
				System.out.println(ad+"\n");
				System.out.println(user.toString());
			}
		} else {
			System.out.println("No ads by this user.");
		}
	}
	public void getAdsByUserId(int id ) {
		Optional<User> user = userRepo.findById(id);
		List<Ad> list=repo.findByUserAd(user);
		if (!list.isEmpty()) {
			for (Ad ad : list) {
				System.out.println("\nThis is the list of ads by user with ID:"+id+"\n");
				System.out.println(ad+"\n");
				System.out.println(user.toString());
			}
		} else {
			System.out.println("No ads by this user.");
		}
	}
}

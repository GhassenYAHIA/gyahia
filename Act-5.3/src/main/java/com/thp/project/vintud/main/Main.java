package com.thp.project.vintud.main;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.thp.project.vintud.config.ApplicationConfiguration;
import com.thp.project.vintud.entity.Ad;
import com.thp.project.vintud.entity.Category;
import com.thp.project.vintud.entity.Role;
import com.thp.project.vintud.entity.Roles;
import com.thp.project.vintud.entity.Search;
import com.thp.project.vintud.entity.Size;
import com.thp.project.vintud.entity.User;
import com.thp.project.vintud.service.AdService;
import com.thp.project.vintud.service.CategoryService;
import com.thp.project.vintud.service.RoleService;
import com.thp.project.vintud.service.SearchService;
import com.thp.project.vintud.service.UserService;

public class Main {

	public static void main(String[] args) {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				ApplicationConfiguration.class);

		// calling beans
		UserService userService = context.getBean(UserService.class);
		RoleService roleService = context.getBean(RoleService.class);
		AdService adService = context.getBean(AdService.class);
		CategoryService categoryService = context.getBean(CategoryService.class);
		SearchService searchService = context.getBean(SearchService.class);

		/* Testing UserService */
		// creating new role
		Role cl = new Role(Roles.CLIENT);
		roleService.createRole(cl);

		// eating new user
		User u1 = new User("firstName", "lastName", " pseudo", "password", "mail", 12, " address", cl);
		User u2 = new User("fName", "lName", " pseudo", "pwd", "mail@", 666, " address", cl);

		// performing create, update , login and updateRole on userService
		userService.createUser(u1);
		userService.createUser(u2);
		userService.login("mail", "password");
		userService.updateUser(u1, "talan@", "new   pseudo");
		Role mod = new Role(Roles.MODERATEUR);
		roleService.createRole(mod);
		userService.updateRole(u1, mod);

		/* Testing SearchService */
		Search s = new Search("veste", Size.M, "Noir", 10, 80);
		searchService.createSearch(s);

		/* Testing AdService */
		// create category
		Date date = new Date();
		Timestamp datePublication = new Timestamp(date.getTime());
		Category cat = new Category("Veste", "vêtement chaud");
		categoryService.createCategory(cat);
		Ad ad = new Ad("veste en cuir", "picture", "veste presque neuve", 100, datePublication, true, "Tunis", 10, u1,
				cat);

		// create Ad in adService
		adService.createAd(ad);
		// get all ads
		adService.viewAds();
		/*
		 * get ads for a defined user 
		 * 1/  first method getAdsByUser uses the attribute
		 * userAd of entity Ad 
		 * 2/  second method getAdsByUserId uses id of entity User,
		 * finds the user and then performs the same solution as the first method
		 **/
		//**********************************
		adService.getAdsByUser(u1);
		//**********************************
		adService.getAdsByUserId(1);
		//**********************************
		adService.getAdsByUserId(2);

	}

}

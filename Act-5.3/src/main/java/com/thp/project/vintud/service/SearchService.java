package com.thp.project.vintud.service;

import org.springframework.stereotype.Repository;

import com.thp.project.vintud.entity.Search;

@Repository
public interface SearchService {

	void createSearch(Search s);
}

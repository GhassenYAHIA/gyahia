package com.thp.project.vintud.service;

import org.springframework.stereotype.Repository;

import com.thp.project.vintud.entity.Category;

@Repository
public interface CategoryService {

	void createCategory(Category category);
}

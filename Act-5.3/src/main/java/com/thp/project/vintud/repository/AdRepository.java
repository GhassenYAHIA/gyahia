package com.thp.project.vintud.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.thp.project.vintud.entity.Ad;
import com.thp.project.vintud.entity.User;

@Repository
@Transactional
public interface AdRepository extends JpaRepository<Ad, Long> {

	List<Ad> findByUserAd(Optional<User> user );
	

	

}

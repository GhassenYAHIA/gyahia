package com.thp.project.vintud.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.thp.project.vintud.entity.User;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Integer> {

	List<User> findByMailAndPassword(String mail, String password);

	

}

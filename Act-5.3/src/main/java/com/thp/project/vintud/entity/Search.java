package com.thp.project.vintud.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "search")
public class Search {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "type")
	private String type;
	@Column(name = "size")
	private Size size;
	@Column(name = "color")
	private String color;
	@Column(name = "min_price")
	private int minPrice;
	@Column(name = "max_price")
	private int maxPrice;

	public Search() {
	}

	public Search(String type, Size size, String color, int minPrice, int maxPrice) {
		this.type = type;
		this.size = size;
		this.color = color;
		this.minPrice = minPrice;
		this.maxPrice = maxPrice;
	}

	public String getType() {
		return type;
	}

	public Size getSize() {
		return size;
	}

	public String getColor() {
		return color;
	}

	public int getMinPrice() {
		return minPrice;
	}

	public int getMaxPrice() {
		return maxPrice;
	}

	public int getId() {
		return id;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setSize(Size size) {
		this.size = size;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setMinPrice(int minPrice) {
		this.minPrice = minPrice;
	}

	public void setMaxPrice(int maxPrice) {
		this.maxPrice = maxPrice;
	}

	public void setIdSearch(int id) {
		this.id = id;
	}

}

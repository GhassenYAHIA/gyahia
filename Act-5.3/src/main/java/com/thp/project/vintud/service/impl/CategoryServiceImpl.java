package com.thp.project.vintud.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thp.project.vintud.entity.Category;
import com.thp.project.vintud.repository.CategoryRepository;
import com.thp.project.vintud.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepository repo;

	public void createCategory(Category category){

		repo.save(category);
		
	}
}

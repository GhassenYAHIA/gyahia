package com.vintud.dao;

import java.sql.SQLException;
import java.util.List;

import com.vintud.entity.*;

public interface IAdDao{
    
    public List<Ad> getAvailableAds();
    public List<Ad> getAdsByCategory(String category);
    public List<Ad> filterAdsByPrice(double min, double max);
    public int create(Ad newAd, int userID, int categoryId) throws SQLException;
    public int update(Ad announce) throws SQLException;
    public int delete(int idTodelete) throws SQLException;
    public List<Ad> filterAdsLocation(String location);
	
	
}

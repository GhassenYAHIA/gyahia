package com.vintud.dao.factory;

import java.sql.Connection;

import com.vintud.dao.IAdDao;
import com.vintud.dao.IFavoriteDao;
import com.vintud.dao.ISearchDao;
import com.vintud.dao.IUserDao;
import com.vintud.dao.impl.AdDaoImpl;
import com.vintud.dao.impl.FavoriteDaoImpl;
import com.vintud.dao.impl.SearchDaoImpl;
import com.vintud.dao.impl.UserDaoImpl;
import com.vintud.db.ConnectionManager;

public class FactoryDao {

	// retourne un objet interagissant avec la base de donn�e
	public IFavoriteDao getFavoriteDao() {
		return new FavoriteDaoImpl(this);
	}

	// retourne un objet interagissant avec la base de donn�e
	public ISearchDao getSearchDao() {
		return new SearchDaoImpl(this);
	}
	// retourne un objet interagissant avec la base de donn�e
	public IAdDao getAdDao() {
		return new AdDaoImpl(this);
	}
	// retourne un objet interagissant avec la base de donn�e
	public IUserDao getUserDao() {
		return new UserDaoImpl(this);
	}

	public static Connection getConnection() {
		return ConnectionManager.getInstance();
	}

}
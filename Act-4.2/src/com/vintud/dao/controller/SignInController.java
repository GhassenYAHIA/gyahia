package com.vintud.dao.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.vintud.dao.factory.FactoryDao;

@WebServlet("/SignInController")
public class SignInController extends HttpServlet {
	String mail;
	String password;
	public static final String VUE = "/vintud/announcements";
	final FactoryDao dao = new FactoryDao();
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher("/WEB-INF/SignIn.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		mail = request.getParameter("mail");
		password = request.getParameter("password");
		

		try {
			if (dao.getUserDao().login(mail, password) == "authorized") {
				HttpSession session = request.getSession();
				session.setAttribute("mail", mail);
				response.sendRedirect(VUE);
			} else
				return;
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

package com.vintud.dao.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vintud.dao.factory.FactoryDao;
import com.vintud.dao.impl.AdDaoImpl;
import com.vintud.entity.Ad;

@WebServlet("/CreateAnnouncementController")
public class CreateAnnouncementController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final FactoryDao dao = new FactoryDao();

	public CreateAnnouncementController() {
		super();

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher("/WEB-INF/createAnnouncement.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String title = request.getParameter("title");
		String description = request.getParameter("description");
		String price = request.getParameter("price");
		String localisation = request.getParameter("localisation");

		Ad announce = new Ad();
		announce.setTitleAd(title);
		announce.setDescriptionAd(description);
		announce.setPriceAd(Integer.parseInt(price));
		announce.setLocalisationAd(localisation);
		AdDaoImpl ad = (AdDaoImpl) dao.getAdDao();
		try {
			ad.create(announce, 1, 1);
		} catch (SQLException e) {

			e.getMessage();
		}

		response.sendRedirect("/vintud/announcement");

	}

}

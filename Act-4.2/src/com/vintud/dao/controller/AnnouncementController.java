package com.vintud.dao.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vintud.dao.factory.FactoryDao;
import com.vintud.dao.impl.AdDaoImpl;
import com.vintud.entity.Ad;



@WebServlet("/AnnouncementController")
public class AnnouncementController extends HttpServlet {
	
	final FactoryDao dao = new FactoryDao();
       
    public AnnouncementController() {
        super();
    }
    
    
    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		AdDaoImpl ad= (AdDaoImpl) dao.getAdDao();
		List<Ad> adList = ad.getAvailableAds();
		request.setAttribute("announcements", adList);
		this.getServletContext().getRequestDispatcher("/WEB-INF/announcements.jsp").forward(request, response);
	}

	

}

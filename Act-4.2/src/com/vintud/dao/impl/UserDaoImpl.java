package com.vintud.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.vintud.dao.IUserDao;
import com.vintud.dao.factory.FactoryDao;
import com.vintud.entity.Roles;
import com.vintud.entity.User;

public class UserDaoImpl  implements IUserDao {
	
	private FactoryDao factoryDao;

	public UserDaoImpl(FactoryDao factoryDao) {
		this.factoryDao = factoryDao;
	}
	
	// méthode affichant les nombres de vues d'une annonce

	public String getViews(int idAd) throws SQLException {
		Connection connect=FactoryDao.getConnection();
		Statement stm = null;
		ResultSet rs = null;
		
			String query = "SELECT  id, title, view_number FROM announcement WHERE id=" + idAd + ";";
			stm = connect.createStatement();
			rs = stm.executeQuery(query);
			String results="";
			while (rs.next())
				results=rs.getString(1) + " - " + rs.getString(2) + " - " + rs.getString(3);

			return results;
	}

	// méthode créant un nouvel utilisateur
	public int create(User user) throws SQLException {
		Connection connect=FactoryDao.getConnection();
		Statement stm = null;
		ResultSet rs = null;
		int lastKey = 1;


			stm = connect.createStatement();

			
			String query = "INSERT INTO `user`( `firstname`, `name`, `pseudo`, `mail`, `u_password`, `phone`, `address`, `role_id`) "
					+ "VALUES ('" + user.getFirstName() + "','" + user.getLastName() + "','" + user.getPseudo() + "','"
					+ user.getMail() + "','" + user.getPassword() + "','" + user.getPhone() + "','" + user.getAddress()
					+ "','" + user.getRoleId() + "')";
			stm.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);

			
			ResultSet keys = stm.getGeneratedKeys();

			while (keys.next()) {
				lastKey = keys.getInt(1);
			}

			user.setIdUser(lastKey);
			return lastKey;

			
	}

	// méthode permettant le login d'un utilisateur

	public String login(String mail, String password) throws SQLException {
		Connection connect=FactoryDao.getConnection();
		Statement stm = null;
		ResultSet rs = null;
		

			String query = "select 1 from user where mail='" + mail + "' and u_password='" + password + "'";
			stm = connect.createStatement();
			rs = stm.executeQuery(query);
			String result="";
			if (rs.next()) {
			result="authorized";
			}
		return result;
	}

	// méthode pour modifier des informations personnelles d'un utilisateur
	public int update(User user) throws SQLException {
		Connection connect=FactoryDao.getConnection();
		Statement stm = null;
		ResultSet rs = null;
		
			stm=connect.createStatement();
			rs = stm.executeQuery("SELECT id FROM user where firstname =  '" + user.getFirstName()+ "' AND name ='"+user.getLastName()+"';");
			rs.next();
			int id = rs.getInt(1);
			user.setIdUser(id);


			String query = "Update user SET pseudo='" + user.getPseudo() + "', mail='" + user.getMail() + "', phone='"
					+ user.getPhone() + "', address='" + user.getAddress() + "' WHERE id=" + user.getIdUser() + ";";
			stm = connect.createStatement();
			 return stm.executeUpdate(query);
		
		
	}

	// méthode pour retrouver les informations d'un vendeur
	public String getBuyerInformation(User user) throws SQLException {
		Connection connect=FactoryDao.getConnection();
		Statement stm = null;
		ResultSet rs = null;
		
			stm=connect.createStatement();
			rs = stm.executeQuery("SELECT id FROM user where firstname =  '" + user.getFirstName()+ "' AND name ='"+user.getLastName()+"';");
			rs.next();
			int id = rs.getInt(1);
			user.setIdUser(id);
			//System.out.println(id+ " this is the id of this user");

			String query = "SELECT u.id,firstname,name,pseudo,mail,phone,address,a.id,a.title FROM user u RIGHT JOIN announcement a ON  u.id=a.user_id WHERE u.id="
					+ user.getIdUser() + ";";
			stm = connect.createStatement();
			rs = stm.executeQuery(query);
			String result="";
			while (rs.next())
				result=rs.getString(1) + " - " + rs.getString(2) + " - " + rs.getString(3) + " - " + rs.getString(4)
								+ " - " + rs.getString(5) + " - " + rs.getString(6) + " - " + rs.getString(7)+" - Ad ID: "+rs.getInt(8)+" - Ad Title: "+rs.getString(9);
		return result;
	}

	// méthode pour modifier le rôle d'un utilisateur

	public int updateRole(User user, Roles roleUser) throws SQLException {
		Connection connect=FactoryDao.getConnection();
		Statement stm = null;
		ResultSet rs = null;

		
			user.setRoleId(roleUser.ordinal());
			stm=connect.createStatement();
			rs = stm.executeQuery("SELECT id FROM user where firstname =  '" + user.getFirstName()+ "' AND name ='"+user.getLastName()+"';");
			rs.next();
			int id = rs.getInt(1);
			user.setIdUser(id);

			String query = "Update user SET role_id=" + (user.getRoleId()+1) + " WHERE id="
					+ user.getIdUser() + ";";
			stm = connect.createStatement();
			 return stm.executeUpdate(query);
		

	}

	public void delete(int i) throws SQLException {

	}



}

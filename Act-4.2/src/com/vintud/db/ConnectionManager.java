package com.vintud.db;

import java.sql.*;

public class ConnectionManager {

	private static Connection instance;

	private ConnectionManager() {

	}

	private static String url = "jdbc:mysql://localhost:3306/vintud?serverTimezone=UTC";

	private static String username = "root";

	private static String password = "";

	// objet Connection

	private static Connection connect;

	// methode qui va retourner notre instance et la crée si elle n'existe pas
	public static Connection getInstance() {
		
		if (connect == null) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");

				connect = DriverManager.getConnection(url, username, password);

			} catch (ClassNotFoundException e) {

				System.out.println("Driver class not found");
				e.getStackTrace();

			} catch (SQLException e) {

				e.getStackTrace();
			}

		}

		return connect;
	}



}
package com.vintud.service;

import com.vintud.entity.Favorite;
import com.vintud.entity.Roles;
import com.vintud.entity.Search;
import com.vintud.entity.User;

public interface IUserService {

	void addFavoriteAd(Favorite favoris, int userId, int announceId);

	void deleteFavorite(int favoriteId);

	void displayFavoriteByIdAnnouncement(int idAnnounce);

	void updateFavorite(Favorite favori);

	void createSearch(Search search);

	void deleteSearch(int id);

	void launchSearch(int id);

	void createUser(User user);
	
	void loginUser(String mail, String password);
	
	void updateUser(User user);
	
	void updateRoleUser(User user,Roles role);
	
	
}
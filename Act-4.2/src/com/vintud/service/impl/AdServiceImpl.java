package com.vintud.service.impl;

import java.sql.SQLException;
import java.util.List;

import com.vintud.dao.factory.FactoryDao;
import com.vintud.entity.Ad;
import com.vintud.entity.Category;
import com.vintud.entity.Favorite;
import com.vintud.entity.User;
import com.vintud.service.IAdService;

public class AdServiceImpl implements IAdService {
    
	private FactoryDao dao = new FactoryDao();

	@Override
	public void getAvailableAds() {
	    List<Ad> ads = dao.getAdDao().getAvailableAds();
	    for (Ad ad : ads) 
	    { 
		System.out.println(ad);
	    }
	 
	}
	@Override
	public void filterAdsByPrice(double min, double max) {
	    List<Ad> ads = dao.getAdDao().filterAdsByPrice(min, max);
	    for (Ad ad : ads) 
	    { 
		System.out.println(ad);
	    }	    
	}
	
	@Override
	public void getAdsByCategory(String category) {
	    List<Ad> ads = dao.getAdDao().getAdsByCategory(category);
	    for (Ad ad : ads) 
	    { 
		System.out.println(ad);
	    }		    
	}

	@Override
	public void filterAdsLocation(String location) {
	    List<Ad> ads = dao.getAdDao().filterAdsLocation(location);
	    for (Ad ad : ads) 
	    { 
		System.out.println(ad);
	    }		    
	}


	@Override
	public void create(Ad newAd, int userID, int categoryId) {
	    int count = 0;
		try {
			count = dao.getAdDao().create(newAd, userID, categoryId);
		} catch (SQLException e) {

			e.getMessage();
		}
		if (count != 0) {
			System.out.println("Ad has been created with success.");

		} else {
			System.out.println("No new ad created.\n");
		}
	}

	@Override
	public void update(Ad announce) {
	    try {

		if (dao.getAdDao().update(announce) == 1) {
			System.out.println("Ad Updated Sccessufly");
		}

	} catch (SQLException e) {
		e.getMessage();
	}
	}

	
	
	@Override
	public void delete(int idTodelete) {
	    try {
		if (dao.getAdDao().delete(idTodelete) == 1) {
			System.out.println("Ad Deleted Successufly !");
		} else {
			System.out.println("Ad Not Found !");

		}
	} catch (SQLException e) {
		e.getMessage();
	}
	    
	}




		
		
		


}
package com.vintud.service.impl;

import java.sql.SQLException;
import java.util.List;

import com.vintud.dao.factory.FactoryDao;
import com.vintud.entity.Favorite;
import com.vintud.entity.Roles;
import com.vintud.entity.Search;
import com.vintud.entity.User;
import com.vintud.service.IUserService;

public class UserServiceImpl implements IUserService {
	private FactoryDao dao = new FactoryDao();

	// permet d'ajouter une favorie
	@Override
	public void addFavoriteAd(Favorite favoris, int userId, int announceId) {
		int count = 0;
		try {
			count = dao.getFavoriteDao().insertFavorite(favoris, userId, announceId);
		} catch (SQLException e) {

			e.getMessage();
		}
		if (count != 0) {
			System.out.println("favoris added successfully");
		} else {
			System.out.println("Failure of adding favorite ! \n");
		}

	}

	// permet de supprimer une favorie
	@Override
	public void deleteFavorite(int favoriteId) {

		try {
			if (dao.getFavoriteDao().delete(favoriteId) == 1) {
				System.out.println("Favorite Deleted Successufly !");
			} else {
				System.out.println("Favorite Not Found !");

			}
		} catch (SQLException e) {
			e.getMessage();
		}
	}

	// permet la mise � jour de la favorie
	@Override
	public void updateFavorite(Favorite favori) {
		try {

			if (dao.getFavoriteDao().update(favori) == 1) {
				System.out.println("Favorite Updated Sccessufly");
			}

		} catch (SQLException e) {
			e.getMessage();
		}
	}

	// permet d'afficher la liste des favoris selon l'id de l'annonce
	public void displayFavoriteByIdAnnouncement(int idAnnounce) {
		try {
			List<Favorite> favoris = dao.getFavoriteDao().findAnnounce(idAnnounce);
			if (favoris.isEmpty()) {
				System.out.println("there is not favorite for the announcment number " + idAnnounce);
			} else {
				System.out.println(
						"the list of the favorite that correspand to the announce number " + idAnnounce + " are : ");
				for (Favorite fav : favoris) {
					System.out.println("the favaorite number " + fav.getIdFavourite() + " is registred on "
							+ fav.getFavouriteDate() + " of the user number " + fav.getFavouriteUser().getIdUser());
				}

			}
		} catch (SQLException e) {

			e.getMessage();
		}
	}

	public void createSearch(Search search) {
		int count = 0;
		try {
			count = dao.getSearchDao().create(search);
		} catch (SQLException e) {

			e.getMessage();
		}
		if (count != 0) {
			System.out.println("New search created.");
		} else {
			System.out.println("No new search created.\n");
		}

	}

	public void deleteSearch(int id) {

		try {
			if (dao.getSearchDao().delete(id) == 1) {
				System.out.println("Search with ID " + id + " was deleted.");
			} else {
				System.out.println("No search with ID " + id + " exists. Verify your informations.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void launchSearch(int id) {

		try {
			String result = dao.getSearchDao().launch(id);
			if (result != "") {
				System.out.println("This is the result of the search requested:\n" + result);
			} else {
				System.out.println("No results for search with ID " + id + ".");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void createUser(User user) {
		int count = 0;
		try {
			count = dao.getUserDao().create(user);
		} catch (SQLException e) {

			e.getMessage();
		}
		if (count != 0) {
			System.out.println("Signing In done.");

		} else {
			System.out.println("No new user created.\n");
		}
	}

	@Override
	public void loginUser(String mail, String password) {
		String testLog = "";
		try

		{
			testLog = dao.getUserDao().login(mail, password);
			if (testLog == "authorized") {
				System.out.println("Login succeeded.");
			} else {
				System.out.println("Verify your information. Login Error.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateUser(User user) {
		int count = 0;
		try {
			count = dao.getUserDao().update(user);
			if (count == 1) {
				System.out.println("Update on user done.");
			} else {
				System.out.println("Update not taken into count.");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateRoleUser(User user, Roles role) {
		int count = 0;
		try {
			count = dao.getUserDao().updateRole(user, role);
			if (count == 1) {
				System.out.println("User role up to date. He/She is now :"+role.name()+".");
			} else {
				System.out.println("Update error. Check your informations.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

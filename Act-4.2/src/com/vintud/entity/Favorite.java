package com.vintud.entity;

import java.sql.Timestamp;

public class Favorite {

	private int idFavourite;
	private Timestamp favouriteDate;
	private User favouriteUser;
	private Ad favouriteAd;

	public Favorite(Timestamp favouriteDate) {

		this.favouriteDate = favouriteDate;
		
	}

	public Favorite() {
	}

	// getter
	public Timestamp getFavouriteDate() {
		return favouriteDate;
	}

	public User getFavouriteUser() {
		return favouriteUser;
	}

	public Ad getFavouriteAd() {
		return favouriteAd;
	}

	public int getIdFavourite() {
		return idFavourite;
	}

	// setter
	public void setFavouriteDate(Timestamp favouriteDate) {
		this.favouriteDate = favouriteDate;
	}

	public void setFavouriteUser(User favouriteUser) {
		this.favouriteUser = favouriteUser;
	}

	public void setFavouriteAd(Ad favouriteAd) {
		this.favouriteAd = favouriteAd;
	}

	public void setIdFavourite(int idFavourite) {
		this.idFavourite = idFavourite;
	}
	
	//cette methode permet d'afficher
	@Override
	public String toString() {
		return "Favorite [idFavourite=" + idFavourite + ", favouriteDate=" + favouriteDate + "]";
	}
	
	
	
}


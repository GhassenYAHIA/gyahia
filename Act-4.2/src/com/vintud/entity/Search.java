package com.vintud.entity;

//enum Size {
//	S, M, L, XL, XXL;
//}

public class Search {

	private String type;
	private Size size;
	private String color;
	private int minPrice;
	private int maxPrice;
	private int idSearch;

	public Search() {
	}

	public Search(String type, Size size, String color, int minPrice, int maxPrice) {
		this.type = type;
		this.size = size;
		this.color = color;
		this.minPrice = minPrice;
		this.maxPrice = maxPrice;
	}

	public String getType() {
		return type;
	}

	public Size getSize() {
		return size;
	}

	public String getColor() {
		return color;
	}

	public int getMinPrice() {
		return minPrice;
	}

	public int getMaxPrice() {
		return maxPrice;
	}

	public int getIdSearch() {
		return idSearch;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setSize(Size size) {
		this.size = size;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setMinPrice(int minPrice) {
		this.minPrice = minPrice;
	}

	public void setMaxPrice(int maxPrice) {
		this.maxPrice = maxPrice;
	}

	public void setIdSearch(int idSearch) {
		this.idSearch = idSearch;
	}

}

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>sign In</title>

<link type="text/css" rel="stylesheet" href="<c:url value="style.css"/>" />
</head>
<body>
	<header>
		<div>
			<fieldset>
				<legend>Navigation</legend>
				<a href="<c:url value="vintud/createAnnouncement"/>">Ajouter
					Annonce</a> <br> <a href="<c:url value="vintud/announcements"/>">Liste
					des annonces</a> <br>
					<a href="<c:url value="vintud/SignIn"/>">Sign In</a>
			</fieldset>

		</div>
	</header>
	
	<h3>Sign In</h3>
	<form class="user-form" method="post" action="SignIn">
		<div class="form-group">
			<label>E-Mail :</label> <input class="form-control" type="email"
				id="mail" name="mail" placeholder="Enter email">
		</div>
		<div class="form-group">
			<label>Password :</label> <input class="form-control" id="password"
				name="password" placeholder="Enter password"></input>
		</div>
		<button class="button-validate" type="submit">Sign In</button>

	</form>
	<ul>

	</ul>

</body>
</html>
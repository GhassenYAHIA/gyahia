<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste des annonces</title>

<link type="text/css" rel="stylesheet" href="<c:url value="style.css"/>" />
</head>
<body>
	<header>
		<div>
			<fieldset>
				<legend>Navigation</legend>
				<a href="<c:url value="vintud/create/announcement"/>">Ajouter
					Annonce</a> <br> <a href="<c:url value="vintud/announcements"/>">Liste
					des annonces</a> <br>
					<a href="<c:url value="vintud/SignIn"/>">Sign In</a>
			</fieldset>

		</div>
	</header>
	<div class="body-content">
		<table class="table">
			<thead class="thead-dark">
				<tr>
					<th scope="col">Title</th>
					<th scope="col">Description</th>
					<th scope="col">Price</th>
					<th scope="col">Category</th>
					<th scope="col">Localisation</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="announce" items="${announcements}">
					<tr>
						<td>${ad.title}</td>
						<td>${ad.description}</td>
						<td>${ad.price}</td>
						<td>${ad.title}</td>
						<td>${ad.location}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>


</body>
</html>
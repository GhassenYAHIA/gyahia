<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link type="text/css" rel="stylesheet" href="<c:url value="style.css"/>" />
<title>Déposer une annonce</title>
</head>
<body>
	<header>
		<div>
			<fieldset>
				<legend>Navigation</legend>
				<a href="<c:url value="vintud/create/announcement"/>">Ajouter
					Annonce</a> <br> <a href="<c:url value="vintud/announcements"/>">Liste
					des annonces</a> <br>
					<a href="<c:url value="vintud/SignIn"/>">Sign In</a>
			</fieldset>

		</div>
	</header>
	<div>
		<form method="post" action="<c:url value="/vintud/createAnnouncement"/>">

			<fieldset>
				<legend>Informations Annonce</legend>
				<br>
				<div class="form-group">
					<label>Title :</label> <input class="form-control" id="title"
						name="title">
				</div>
				<br>
				<div class="form-group">
					<label>Description :</label> <input type="text"
						class="form-control" id="description" name="description"
						size="150">
				</div>
				<br>
				<div class="form-group">
					<label>Price :</label> <input class="form-control" id="price"
						name="price"></input>
				</div>
				<br>
				<div class="form-group">

					<label>Category :</label> <select class="form-control"
						name="category">
						<option value="T-shirt">T-shirt</option>
						<option value="pantalon">Pantalon</option>
						<option value="veste">Veste</option>

					</select> <br>

					<div class="form-group">
						<label>Localisation :</label> <input class="form-control"
							id="localisation" name="localisation"></input>
					</div>
					<button type="submit" class="button-validate">Create</button>
				</div>
			</fieldset>
		</form>
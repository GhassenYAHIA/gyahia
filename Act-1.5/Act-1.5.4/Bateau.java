public class Bateau {
	
	static int ID;
	// attributs 1.5.4
	private Coque coque;
	private Pont pont;
	private Cabine cabine;

	// constructeur par défaut
	public Bateau() {
		ID=1;
		toString();
		coque = new Coque();
		pont = new Pont();
		cabine = new Cabine();
		
	}

	// constructeur avec paramètres
	public Bateau(int cabCapacite, String cabClasse, String couleurCoque,
			String materiauCoque, String materiauPont, double surfacePont) {
		this.cabine.setCapacite(cabCapacite);
		this.cabine.setClasseCabine(cabClasse);
		this.coque.setCouleur(couleurCoque);
		this.coque.setMateriau(materiauCoque);
		this.pont.setMateriau(materiauPont);
		this.pont.setSurface(surfacePont);
	}

	//Accesseurs et Mutateurs pour les autres classes
	public Cabine getCabine() {
		return cabine;
	}

	public void setCabine(int capacite, String classeCabine) {
		this.cabine.setCapacite(capacite);
		this.cabine.setClasseCabine(classeCabine);
	}
	
	public Coque getCoque() {
		return coque;
	}
	public void setCoque(String couleur,String materiau) {
		this.coque.setCouleur(couleur);
		this.coque.setMateriau(materiau);
	}
	public Pont getPont() {
		return pont;
	}
	public void setPont(String materiau,double surface) {
		this.pont.setMateriau(materiau);
		this.pont.setSurface(surface);
	}
	
	public String toString() {
		return "Le bateau n° "+ Bateau.ID + " est constitué de:"
		+"\n La coque n° "+Coque.ID+"\n La cabine n° "+Cabine.ID+"\n Le pont n° "+Pont.ID+" est consituté du Mat n° "+Mat.ID+" qui est constitué de la Voile n° "+Voile.ID;

	}
}
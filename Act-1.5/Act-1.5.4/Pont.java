public class Pont {
	private String materiau;
	private double surface;
	static int ID;
	private Mat mat;

	public Pont() {
		materiau = "Inconnu";
		surface = 0;
		mat = new Mat();
		ID=4;
		toString();
		
	}

	public Pont(String materiau, double surface, double hauteurMat, int nombreMat) {
		this.materiau = materiau;
		this.surface = surface;
		this.mat.setHauteur(hauteurMat);
		this.mat.setNombre(nombreMat);
	}

	public String getMateriau() {
		return materiau;
	}

	public double getSurface() {
		return surface;
	}

	public void setSurface(double sSurface) {
		this.surface = sSurface;
	}

	public void setMateriau(String sMateriau) {
		this.materiau = sMateriau;
	}

	public String toString() {
		return "Le pont n°" + Pont.ID + " est consituté du Mat n°" + Mat.ID
				+ " qui est constitué de la Voile N° " + Voile.ID;
	}

	
}
public class Voile {
	private double longueur;
	private double largeur;
	public String couleur;
	protected String materiau;
	static int ID;

	private boolean enroule;
	private boolean deroule;

	// constructeur par défaut
	public Voile() {
		longueur = 0;
		largeur = 0;
		couleur = "Inconnu";
		materiau = "Inconnu";
		enroule = false;
		deroule = true;
		ID=6;
		toString();
		
	}

	// constructeur avec paramètres
	public Voile(double longueur, double largeur, String couleur, String materiau) {
		this.longueur = 0;
		this.largeur = 0;
		this.couleur = "Inconnu";
		this.materiau = "Inconnu";
	}

	//
	public void enrouler() {
		this.enroule = true;
		this.deroule = false;
	}

	public void derouler() {
		this.enroule = false;
		this.deroule = true;
	}

	// Accesseurs
	public double getLongueur() {
		return longueur;
	}

	public double getLargeur() {
		return largeur;
	}

	public String getCouleur() {
		return couleur;
	}

	public String getMateriau() {
		return materiau;
	}

	// Mutateurs
	public void setLongueur(double sLongueur) {
		this.longueur = sLongueur;
	}

	public void setLargeur(double sLargeur) {
		this.largeur = sLargeur;
	}

	public void setCouleur(String sCouleur) {
		this.couleur = sCouleur;
	}

	public void setMateriau(String sMateriau) {
		this.materiau = sMateriau;
	}

	
}

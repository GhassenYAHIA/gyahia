public class Coque {
	private String couleur;
	private String materiau;
	static int ID;

	public Coque() {
		couleur = "Inconnue";
		materiau = "Inconnu";
		ID=2;
		toString();
	}

	public Coque(String couleur, String materiau) {
		this.couleur = couleur;
		this.materiau = materiau;
	}

	public String getCouleur() {
		return couleur;
	}

	public String getMateriau() {
		return materiau;
	}

	public void setCouleur(String sCouleur) {
		this.couleur = sCouleur;
	}

	public void setMateriau(String sMateriau) {
		this.materiau = sMateriau;
	}
	
	
}
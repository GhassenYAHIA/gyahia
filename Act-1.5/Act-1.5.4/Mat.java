public class Mat {

	private double hauteur;
	private int nombre;
	private Voile voile;
	static int ID;

	public Mat() {
		hauteur = 0;
		nombre = 0;
		voile = new Voile();
		ID=5;
		toString();
		
	}

	public Mat(double hauteur, int nombre,double longueurV, double largeurV,String couleurV,String materiauV) {
		this.hauteur = hauteur;
		this.nombre = nombre;
		this.voile.setLongueur(longueurV);
		this.voile.setLargeur(largeurV);
		this.voile.setCouleur(couleurV);
		this.voile.setMateriau(materiauV);

	}

	public double getHauteur() {
		return hauteur;
	}

	public int getNombre() {
		return nombre;
	}

	public void setHauteur(double hauteur) {
		this.hauteur = hauteur;
	}

	public void setNombre(int nombre) {
		this.nombre = nombre;
	}

	public double vitesseBateau() {
		double vitesseInit = 10;
		double vitesse;
		return vitesse = vitesseInit * this.nombre;

	}

	

}

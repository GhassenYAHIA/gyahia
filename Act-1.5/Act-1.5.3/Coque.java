public class Coque {
	public String couleur;
	private String materiau;
	static int ID;

	public Coque() {
		couleur = "Inconnue";
		materiau = "Inconnu";
		ID = 2;
	}

	public Coque(String couleur, String materiau) {
		this.couleur = couleur;
		this.materiau = materiau;
	}

	public String getCouleur() {
		return couleur;
	}

	public String getMateriau() {
		return materiau;
	}

	public void setCouleur(String sCouleur) {
		this.couleur = sCouleur;
	}

	public void setMateriau(String sMateriau) {
		this.materiau = sMateriau;
	}

	public String toString() {
		return "La coque du bateau est à base de " + this.materiau + " et a une couleur " + this.couleur + ".";
	}
}
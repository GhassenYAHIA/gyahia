public class Cabine {

	private int capacite;
	private String classeCabine;
	static int ID;

	public Cabine() {
		capacite = 0;
		classeCabine = "Inconnue";
		ID = 4;
	}

	public Cabine(int capacite, String classeCabine) {
		this.capacite = capacite;
		this.classeCabine = classeCabine;
	}

	public int getCapacite() {
		return capacite;
	}

	public String getClasseCabine() {
		return classeCabine;
	}

	public void setCapacite(int sCapacite) {
		this.capacite = sCapacite;
	}

	public void setClasseCabine(String sClasseCabine) {
		this.classeCabine = sClasseCabine;
	}

	public String toString() {
		return "Chaque cabine de " + this.classeCabine + " classe a une capacité de " + this.capacite + ".";
	}

}

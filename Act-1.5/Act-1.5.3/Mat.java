public class Mat {

	private double hauteur;
	private int nombre;
	static int ID;

	public Mat() {
		hauteur = 0;
		nombre = 0;
		ID = 5;

	}

	public Mat(double hauteur, int nombre) {
		this.hauteur = hauteur;
		this.nombre = nombre;

	}

	public double getHauteur() {
		return hauteur;
	}

	public int getnombre() {
		return nombre;
	}

	public void setHauteur(double hauteur) {
		this.hauteur = hauteur;
	}

	public void setnombre(int nombre) {
		this.nombre = nombre;
	}

	public double vitesseBateau() {
		double vitesseInit = 10;
		double vitesse;
		return vitesse = vitesseInit * this.nombre;

	}

	public String toString() {
		return "Sur le bateau nous avons " + this.nombre + " mats d'une hauter de " + this.hauteur + ".";
	}

}

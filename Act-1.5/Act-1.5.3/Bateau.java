public class Bateau{
	public String nom;
	private String constructeur;
	static int ID;
	//constructeur par défaut
	public Bateau(){
		System.out.println("Caractéristiques de mon bateau!");
		nom="Inconnu";
		constructeur="Inconnu";
		ID=1;
	}
	//constructeur avec paramètres
	public Bateau(String nom, String constructeur){
		this.nom=nom;
		this.constructeur=constructeur;
	}
	//Accesseurs
	public String getNom(){
		return nom;
	}
	public String getConstructeur(){
		return constructeur;
	}
	//Mutateurs
	public void setNom(String sNom){
		this.nom=sNom;
	}
	public void setConstructeur(String sConstructeur){
		this.constructeur=sConstructeur;
	}
	public void cararteristiques(){
		System.out.println("Ce bateau porte le nom "+this.nom+".");
	}
	/**principal
	public static void main(String[] args){

	}*/
}
public class Pont {
	private String materiau;
	private double surface;
	static int ID;
	
	public Pont() {
		materiau = "Inconnu";
		surface = 0;
		ID = 3;
	}
	public Pont(String materiau, double surface) {
		this.materiau = materiau;
		this.surface = surface;
	}
	public String getMateriau() {
		return materiau;
	}
	public double getsurface() {
		return surface;
	}
	public void setsurface(double sSurface) {
		this.surface = sSurface;
	}
		public void setMateriau(String sMateriau) {
		this.materiau = sMateriau;
	}
	public String toString() {
		return "Le pont du bateau est fait à partir de"+this.materiau+" et il a une surface de "+this.surface+".";
	}
}
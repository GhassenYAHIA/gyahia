public class Roman extends Livre {
		
	private int nbChapitre;
	private String description;
	
	public Roman(){
		super();
		nbChapitre = 0;
		description = "vide";
			
	}
		
	public Roman(String titre,  String auteur, double prix, int nPage){
		super(titre, auteur, prix, nPage);
	}
	
	public int getNbChapitre() {
		return nbChapitre;
	}
	public String getDescription() {
		return description;
	}

	public void setNbChapitre(int nbChapitre) {
		this.nbChapitre = nbChapitre;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void Afficher()
	{	
		super.Afficher();
		System.out.println("Ce roman contient " + this.nbChapitre + " chapitre(s). \nRésumé du roman :\n" + this.description);
	}


}

public class Recette {
	private String nom;
	private String description;
	private int niveauDiff;

	private String[] etapes = new String[10];
	private String[] astuces = new String[10];
	static int i;
	static int j;


	public Recette() {
		nom = "Inconnu";
		description = "Inconnue";
		niveauDiff = 0;
	}

	public Recette(String nom, String description, int niveauDiff) {
		this.nom = nom;
		this.description = description;
		this.niveauDiff = niveauDiff;
	}

	public String getNom() {
		return nom;
	}

	public String getDescription() {
		return description;
	}

	public int getNiveauDiff() {
		return niveauDiff;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setNiveauDiff(int niveauDiff) {
		this.niveauDiff = niveauDiff;
	}

	//getters et setters pour etapes et astuces
	public String[] getEtapes() {
		return etapes;
	}

	public String[] getAstuces() {
		return astuces;
	}

	public void setEtapes(String[] etapes) {
		this.etapes = etapes;
	}
	public void setAstuce(String[] astuces) {
		this.astuces = astuces;
	}	
	public void addAstuce(String astuce) {
		astuces[i] = astuce;
		i++;
	}
	public void addEtape(String etape) {
		etapes[j] = etape;
		j++;
	}


	public void Afficher() {
		System.out.println("Recette : \"" + this.nom + "\" de difficulté " + this.niveauDiff+".\nDescription: "+this.description);
		System.out.println("Etapes : ");
		for(int k = 0 ; k<j ; k++)
		{
			System.out.println((k+1)+"/ "+ this.etapes[k]);
		}
		
		System.out.println("Astuces : ");
		for(int k = 0 ; k<i ; k++)
		{
			System.out.println((k+1)+"/ " + this.astuces[k]);
		}
		
	
	}

}
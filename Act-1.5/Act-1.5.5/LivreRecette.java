public class LivreRecette extends Livre{
	private Recette[] recettes=new Recette[10];
	static int i;

	public LivreRecette(){
		super();
	
	}
	public LivreRecette(String titre,  String auteur, double prix, int nPage){
		super(titre, auteur, prix, nPage);
	}
	public void addRecette(Recette rc) {
		recettes[i] = rc;
		i++;
	}
	public void Afficher(){
		super.Afficher();
		System.out.println("Il s'agit d'un livre de recettes de cuisine comportant "+i+" recettes.");
		for(int k = 0 ; k<i ; k++)
		{
			System.out.println((k+1)+"/ "+ this.recettes[k].getNom()+".");
		}
	}
}
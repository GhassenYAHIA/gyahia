public class Livre {
	private String titre;
	private String auteur;
	private double prix;
	private int nPage;
	private String genre;
	private boolean isTrue;

	
	public Livre(){
		titre = "Inconnu";
		auteur = "Inconnu";
		prix = 0;
		nPage = 0;
		genre = "Inconnu";
	}
	
	public Livre(String titre,  String auteur, double prix, int nPage){
		this.titre = titre;
		this.auteur = auteur;
		this.prix = prix;
		this.nPage = nPage;
		
	}
	
	//getters
	public String getTitre() {
		return titre;
	}
	public String getAuteur() {
		return auteur;
	}
	public double getPrix() {
		return prix;
	}
	public int getNPage() {
		return nPage;
	}
	public String getGenre() {
		return genre;
	}

	//setters
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	public void setNPage(int nPage) {
		this.nPage = nPage;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	

	public boolean isTrue() {
		return isTrue;
	}

	public void setTrue(boolean isTrue) {
		this.isTrue = isTrue;
	}
	
	public void Afficher() {
		System.out.println("*********************\nLe titre du livre est " + this.titre + " écrit par " + this.auteur + ". \nIl contient " +  this.nPage + " pages et il coûte " + this.prix+ " DT.");
	}

}

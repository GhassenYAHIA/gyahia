public class Manga extends Livre{

	public Manga(){
		super();
			
	}
		
	public Manga(String titre,  String auteur, double prix, int nPage){
		super( titre, auteur, prix, nPage);
	}
	
	public void Afficher()
	{	
		super.Afficher();
		System.out.println("Ce livre est un manga");
		System.out.println("N'oubliez pas que les mangas se lisent de droite à gauche.");
	}
}

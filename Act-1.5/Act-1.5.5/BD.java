public class BD extends Livre {
	private boolean msg;
	
	public BD(){
		super();
		msg = false;		
	}
		
	public BD(String titre,  String auteur, double prix, int nPage, boolean msg){
		super( titre,   auteur,  prix,  nPage);
		this.msg=msg;	
	}
	public boolean getMsg(){
		return msg;
	}
	public void setMsg(boolean msg){
		this.msg=msg;
	}
	public void Afficher(){
		
		super.Afficher();
		System.out.println("Il s'agit d'une bande dessinée.");
		if(this.msg == false)
		{
			System.out.println("Cette BD est en N&B.");
		}else {
			System.out.println("Cette BD est en couleurs.");	}
	}		
	
}

import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../post';
import { PostService } from '../post.service';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {
  @Input() post: Post

  constructor(private postService: PostService) { }

  ngOnInit() { }

  onLike() {
    this.post.loveIts++;
  }

  onDislike() {
    this.post.loveIts--;

  }

}

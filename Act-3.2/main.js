alert("Veuillez remplir le formulaire.");

function ValidationEvent() {
  var erreur = document.getElementById("errorMsg");
  var email = document.getElementById("email").value;
  var bday = document.getElementById("bday").value;


  if (bday != '') {
    bday = new Date(bday);
    var today = new Date();
    var age = Math.floor((today - bday) / (365.25 * 24 * 60 * 60 * 1000));
  }

  if (age < 18) {
    erreur.innerHTML = "Vous avez " + age + "ans. Vous êtes encore mineur. ";
    $("#errorMsg").css('color', 'red');
    return false;
  } else if (email != "") {
    erreur.innerHTML = "Vous avez tout rempli et tout est en ordre!";
    $("#errorMsg").css('color', 'green');
    setTimeout(function() {
      alert("Votre formulaire a été transmis.");
    }, 2000);
    return true;
  } else {
    erreur.innerHTML = "Adresse mail non communiquée.";
    $("#errorMsg").css('color', 'goldenrod');
    return false;
  }



}


/*
  setTimeout(function() {
    return true;
  }, 3000);
  */


/*
    jQuery.extend(jQuery.validator.messages, {
      required: "Ce champs est requis.",
      email: "Veuillez saisir une adresse mail valide.",
      date: "Veuillez saisir une date valide",
      maxlength: jQuery.validator.format("Nombre de caractères maximum est de {0}."),

    });
*/

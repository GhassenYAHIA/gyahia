package com.thp.spring.simplecontext.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.thp.spring.simplecontext.entity.Bateau;

@Configuration
public class AppBateauConfig {
@Bean
public Bateau bateau() {
	Bateau bateau=new Bateau();
	bateau.setNom("Titanic");
	bateau.setType("Paquebot");
	bateau.setTaille(200);
	return bateau;
}


}

package com.thp.spring.simplecontext.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.thp.spring.simplecontext.entity.Bateau;
import com.thp.spring.simplecontext.entity.Moussaillon;

@Repository
public interface BateauRepository extends JpaRepository<Bateau,Long> {
	int create(int id, String name, String type, double taille);

	int create(Bateau bateau);

	Bateau update(Bateau bateau, String name, String type, double taille);

	void delete(Bateau bateau);

	Bateau findById(int id);

	Bateau findByMoussaillon(Moussaillon moussaillon);

	List<Bateau> findAll();
}

package com.thp.spring.simplecontext.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
	public class HelloController {

		@RequestMapping("/")
		public String index() {
			return "Si tu vois ça c'est que tu es sur le bon chemin!";
		}
}

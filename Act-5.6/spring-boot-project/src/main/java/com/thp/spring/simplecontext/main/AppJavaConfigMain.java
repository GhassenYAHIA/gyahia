package com.thp.spring.simplecontext.main;


import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.thp.spring.simplecontext.config.ApplicationConfiguration;
import com.thp.spring.simplecontext.entity.Moussaillon;
import com.thp.spring.simplecontext.repository.BateauRepository;
import com.thp.spring.simplecontext.repository.MoussaillonRepository;

public class AppJavaConfigMain {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				ApplicationConfiguration.class);

		// calling beans
		BateauRepository bateauRepo = context.getBean(BateauRepository.class);
		MoussaillonRepository moussaillonRepo = context.getBean(MoussaillonRepository.class);
		
			}
}

package com.thp.spring.simplecontext.service;

import org.springframework.stereotype.Repository;

import com.thp.spring.simplecontext.dto.BateauDTO;


public interface BateauService {

	BateauDTO findById(int id);

}

package com.thp.spring.simplecontext.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.thp.spring.simplecontext.dto.MoussaillonDTO;
import com.thp.spring.simplecontext.service.MoussaillonService;

@RestController
public class MoussaillonController {

	//@Autowired
	MoussaillonService mServ;

	@GetMapping(path = "/moussaillon/{moussaillonId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody MoussaillonDTO findById(@PathVariable int moussaillonId) {
		return mServ.getMoussaillonById(moussaillonId);

	}

}

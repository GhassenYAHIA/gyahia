package com.thp.spring.simplecontext.service;

import com.thp.spring.simplecontext.dto.MoussaillonDTO;
import com.thp.spring.simplecontext.entity.Moussaillon;

public interface MoussaillonService {

	MoussaillonDTO getMoussaillonById(int id);

	Moussaillon create(Moussaillon moussaillon);

	Moussaillon update(Moussaillon moussaillon, String firstName, String lastName, String config);

	void delete(Moussaillon moussaillon);

	}

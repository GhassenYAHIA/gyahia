package com.thp.spring.simplecontext.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "moussaillon")
public class Moussaillon implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "config")
	private String config;

	/*
	 * Many to one ralation This means that moussaillon entity will have a foreign
	 * key column named "bateau_id" referring to primary attribute id of entity
	 * bateau
	 */
	@ManyToOne
	@JoinColumn(name = "bateau_id")
	private Bateau bateau;

	public Moussaillon() {
	}

	public Moussaillon(String firstName, String lastName, String config, Bateau bateau) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.config = config;
		this.bateau = bateau;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getConfig() {
		return config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	public int getId() {
		return id;
	}

	public Bateau getBateau() {
		return bateau;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setBateau(Bateau bateau) {
		this.bateau = bateau;
	}

	@Override
	public String toString() {
		return "Moussaillon [id=" + this.getId() + ", firstName=" + this.getFirstName() + ", lastName="
				+ this.getLastName() + ", config=" + this.getConfig() + ", bateau=" + this.getBateau() + "]";
	}

}

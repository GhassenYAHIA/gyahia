package com.thp.spring.simplecontext.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.thp.spring.simplecontext.dto.ModelMapperConverter;
import com.thp.spring.simplecontext.dto.MoussaillonDTO;
import com.thp.spring.simplecontext.entity.Moussaillon;
import com.thp.spring.simplecontext.repository.MoussaillonRepository;
import com.thp.spring.simplecontext.service.MoussaillonService;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class MoussaillonServiceImpl implements MoussaillonService {

	//@Autowired
	MoussaillonRepository mRepo;

	@Override
	public Moussaillon create(Moussaillon moussaillon) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Moussaillon update(Moussaillon moussaillon, String firstName, String lastName, String config) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Moussaillon moussaillon) {
		// TODO Auto-generated method stub

	}

	
	@Override
	public MoussaillonDTO getMoussaillonById(int id) {
		Moussaillon moussaillonEntity = mRepo.findById(id);
		MoussaillonDTO moussaillonDTO=ModelMapperConverter.convertToDTO(moussaillonEntity, MoussaillonDTO.class);
		return moussaillonDTO;

	}


}
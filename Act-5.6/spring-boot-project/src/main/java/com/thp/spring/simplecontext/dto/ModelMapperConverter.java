package com.thp.spring.simplecontext.dto;

import org.modelmapper.ModelMapper;

import com.thp.spring.simplecontext.entity.Bateau;
import com.thp.spring.simplecontext.entity.Moussaillon;

public class ModelMapperConverter {

	
	
	public static BateauDTO convertToDTO(Bateau bateau, Class<BateauDTO> class1) {
		ModelMapper modelMapper = new ModelMapper();
		BateauDTO bDTO= modelMapper.map(bateau, BateauDTO.class);
		return bDTO;
	}
	
	
	public static MoussaillonDTO convertToDTO(Moussaillon m, Class<MoussaillonDTO> class1) {
		ModelMapper modelMapper = new ModelMapper();
		MoussaillonDTO mDTO= modelMapper.map(m, MoussaillonDTO.class);
		return mDTO;
	}

}

package com.thp.spring.simplecontext.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.thp.spring.simplecontext.entity.Moussaillon;

@Repository
public interface MoussaillonRepository extends JpaRepository<Moussaillon, Long> {
	Optional<Moussaillon> findByIdMoussaillon(int idMousaillon);

	Moussaillon findById(int id);

	
}

package com.thp.spring.simplecontext.dto;

public class MoussaillonDTO {

	private int id;
	private String firstName;
	private String lastName;
	private String config;
	private String bateau;

	public MoussaillonDTO(String firstName, String lastName, String config, String bateau) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.config = config;
		this.bateau = bateau;
	}

	public int getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getConfig() {
		return config;
	}

	public String getBateau() {
		return bateau;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	public void setBateau(String bateau) {
		this.bateau = bateau;
	}

}

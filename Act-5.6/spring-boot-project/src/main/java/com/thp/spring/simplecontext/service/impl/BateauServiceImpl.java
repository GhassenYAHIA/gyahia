package com.thp.spring.simplecontext.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.thp.spring.simplecontext.dto.BateauDTO;
import com.thp.spring.simplecontext.dto.ModelMapperConverter;
import com.thp.spring.simplecontext.entity.Bateau;
import com.thp.spring.simplecontext.repository.BateauRepository;
import com.thp.spring.simplecontext.service.BateauService;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class BateauServiceImpl implements BateauService {

	//@Autowired
	private BateauRepository bRepo;

	@Override
	public BateauDTO findById(int id) {
		Bateau bateauEntity = bRepo.findById(id);
		return ModelMapperConverter.convertToDTO(bateauEntity, BateauDTO.class);

	}

}
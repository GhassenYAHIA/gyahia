abstract class Vehicule {
	private int matricule;
	private int anneeModele;
	private double prix;
	private int puissance;
	static int nb = 0;

	public Vehicule(int anneeModele, int matricule, int puissance, double prix) {
		this.anneeModele = anneeModele;
		this.matricule = ++nb;
		this.puissance = puissance;
		this.prix = prix;
	}

	//
	public int getAnneeModele() {
		return this.anneeModele;
	}

	public void setAnneeModele(int anneeModele) {
		this.anneeModele = anneeModele;
	}

	public int getMatricule() {
		return this.matricule;
	}

	public void setMatricule(int matricule) {
		this.matricule = matricule;
	}

	public double getPrix() {
		return this.prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public int getPuissance() {
		return this.puissance;
	}

	public void setPuissance(int puissance) {
		this.puissance = puissance;
	}

	abstract void demarrer();

	abstract void accelerer();

	public String toString() {
		String chaine = "";
		if (this.getClass().getName() == "Voiture") {
			chaine = "Cette voiture";
		} else {
			chaine = "Ce camion";
		}
		return chaine + " de " + this.anneeModele + ", matricule n° " + this.matricule + ", d'une puissance de "
				+ this.puissance + " CV et au prix de " + this.prix + " euros a été enregistrée.";
	}

}

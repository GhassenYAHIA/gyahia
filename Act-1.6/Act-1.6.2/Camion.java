public class Camion extends Vehicule {

	public Camion(int anneeModele, int matricule, int puissance, double prix) {
		super(anneeModele, matricule, puissance, prix);
	}

	void demarrer() {
		System.out.println(
				"Le camion de " + this.getAnneeModele() + ", matricule n°" + this.getMatricule() + " démarre!");
	}

	void accelerer() {
		System.out.println(
				"Le camion de " + this.getAnneeModele() + ", matricule n°" + this.getMatricule() + " accélère!");

	}

}

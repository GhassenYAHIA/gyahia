public class Voiture extends Vehicule {

	public Voiture(int anneeModele, int matricule, int puissance, double prix) {
		super(anneeModele, matricule, puissance, prix);
	}

	void demarrer() {
		System.out.println(
				"La voiture de " + this.getAnneeModele() + ", matricule n°" + this.getMatricule() + " démarre!");
	}

	void accelerer() {
		System.out.println(
				"La voiture de " + this.getAnneeModele() + ", matricule n°" + this.getMatricule() + " accélère!");

	}
}

import java.util.Arrays;

public class PlaylisteImpl implements Playliste {
	final int MAX_MUSIQUES = 100;
	private String nom;
	private String genre;
	private int nombreTitres;
	private Musique[] tableauMusique = new MusiqueImpl[MAX_MUSIQUES];
	private int id;
	static int nbPlaylistes = 0;

	public PlaylisteImpl(String nom, String genre, int nombreTitres) {
		this.nom = nom;
		this.genre = genre;
		this.nombreTitres = nombreTitres;
		this.id = ++nbPlaylistes;
	}

	// getters
	public String getNom() {
		return nom;
	}

	public String getGenre() {
		return genre;
	}

	public int getNombreTitres() {
		return nombreTitres;
	}

	public Musique[] getTableauMusique() {
		return tableauMusique;
	}

	// setters
	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public void setNombreTitres(int nombreTitres) {
		this.nombreTitres = nombreTitres;
	}

	public void setTableauMusique(Musique[] tableauMusique) {
		this.tableauMusique = tableauMusique;
	}

	// methodes de l'interface

	// affichage playliste et ses caractéristiques

	public String toString() {
		String chaine = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nLa playlist n°" + this.id + " s\'appelle " + this.nom
				+ ".\nElle contient " + this.nombreTitres + " chansons de genre " + this.genre
				+ ".\nElle contient les titres suivants: \n";
		for (int i = 0; i < this.nombreTitres; i++) {
			chaine = chaine + this.tableauMusique[i] + "\n\n";
		}
		;
		return chaine;
	}

	// ajout d'une chanson
	public void ajouterMusique(Musique chanson) {
		this.tableauMusique[this.nombreTitres] = chanson;
		this.nombreTitres++;
	}

	// suppression doublon

	public Musique[] supprimerDoublon() {
		for (int i = 0; i < this.nombreTitres; i++) {
			for (int j = i + 1; j < this.nombreTitres; j++) {
				if (this.tableauMusique[i].equals(this.tableauMusique[j])) {
					for (int k = j; k < this.nombreTitres; k++) {
						this.tableauMusique[k] = this.tableauMusique[j + 1];
					}

					this.nombreTitres--;
				}
			}
		}
		return this.tableauMusique;
	}

	public void afficherMemeMusique(PlaylisteImpl playlist) {

		for (int i = 0; i < this.nombreTitres; i++) {
			for (int j = 0; j < playlist.nombreTitres; j++) {
				if (this.tableauMusique[i].equals(playlist.tableauMusique[j])) {
					System.out.println(
							"Ce titre : " + playlist.tableauMusique[i] + " est présent dans les deux playslistes.");
				}
			}
		}
	}

}

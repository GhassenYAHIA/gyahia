import java.util.ArrayList;

public class Library {
	private int id;
	private String name;
	private String address;
	private String numTel;
	private ArrayList<Book> tabBook;
	private ArrayList<Rent> tabRent;

	public Library(int id, String name, String address, String numTel) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.numTel = numTel;
		//this.tabBook = tabBook;
		//this.tabRent = tabRent;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNumTel() {
		return numTel;
	}

	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}

	public ArrayList<Book> getTabBook() {
		return tabBook;
	}

	public void setTabBook(ArrayList<Book> tabBook) {
		this.tabBook = tabBook;
	}

	public ArrayList<Rent> getTabRent() {
		return tabRent;
	}

	public void setTabRent(ArrayList<Rent> tabRent) {
		this.tabRent = tabRent;
	}

}

public class Rent {
	private int id;
	private User user;
	private Book book;
	private String datePret;
	private String dateFin;

	public Rent(int id, User user, Book book, String datePret, String dateFin) {
		this.id = id;
		this.user = user;
		this.book = book;
		this.datePret = datePret;
		this.dateFin = dateFin;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public String getDatePret() {
		return datePret;
	}

	public void setDatePret(String datePret) {
		this.datePret = datePret;
	}

	public String getDateFin() {
		return dateFin;
	}

	public void setDateFin(String dateFin) {
		this.dateFin = dateFin;
	}

	
	public String toString() {
		return "Rent [id=" + id + ", user=" + user + ", book=" + book + ", datePret=" + datePret + ", dateFin="
				+ dateFin + "]";
	}

	
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rent other = (Rent) obj;
		if (book == null) {
			if (other.book != null)
				return false;
		} else if (!book.equals(other.book))
			return false;
		if (dateFin == null) {
			if (other.dateFin != null)
				return false;
		} else if (!dateFin.equals(other.dateFin))
			return false;
		if (datePret == null) {
			if (other.datePret != null)
				return false;
		} else if (!datePret.equals(other.datePret))
			return false;
		if (id != other.id)
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}
	
}
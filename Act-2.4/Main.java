import java.sql.*;

public class Main {

	public static void main(String[] args) throws SQLException {

		LibraryManager libManager = new LibraryManager();
		libManager.searchBookAuthor("Elisabeth Freeman");
		System.out.println("**********New Query**********");
		libManager.searchBookId(4);
		System.out.println("**********New Query**********");
		libManager.sortBookAuthor();
		System.out.println("**********New Query**********");
		libManager.listBookLibrary("Bibliothèque du village");
		System.out.println("**********New Query**********");
		libManager.searchUserId(1);
		System.out.println("**********New Query**********");
		libManager.listBookRent("Bibliothèque du village");
		System.out.println("**********New Query**********");
		//libManager.listBookRentUser();
		System.out.println("**********New Query**********");
		Library l= new Library(1,"Bibliothque du village","05 chemin du village","0531256897"); 
		Book b1=new Book(5,"Test Title","Test Author","editor", 500, "Not quite sure yet.", l);	
	libManager.addBook(b1);
		libManager.listBookLibrary("Bibliothèque du village");
	}
}

import java.util.Arrays;import java.util.ArrayList;

public class User {
	private String id;
	private String prenom;
	private String nom;
	private String address;
	private String numTel;
	private String mail;
	private ArrayList<Rent> tabRent;

	public User(String id, String prenom, String nom, String address, String numTel, String mail) {
		this.id = id;
		this.prenom = prenom;
		this.nom = nom;
		this.address = address;
		this.numTel = numTel;
		this.mail = mail;
		//this.tabRent = tabRent;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNumTel() {
		return numTel;
	}

	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public ArrayList<Rent> getTabRent() {
		return tabRent;
	}

	public void setTabRent(ArrayList<Rent> tabRent) {
		this.tabRent = tabRent;
	}

	public String toString() {
		return "User [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", address=" + address + ", numTel=" + numTel
				+ ", mail=" + mail + "]";
	}

	public boolean equals(User obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (mail == null) {
			if (other.mail != null)
				return false;
		} else if (!mail.equals(other.mail))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (numTel != other.numTel)
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		return true;
	}

}
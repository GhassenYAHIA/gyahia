import java.sql.*;

public class LibraryManager {
	private Connection con;
	private Statement stm;
	ResultSet rs;

	public LibraryManager() {

		try {
			con = ConnectionManager.getConnection();
			stm = con.createStatement();

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	// Fonctionnent
	// tri livres de la bibliothèque par auteur
	public void searchBookAuthor(String author) {
		try {

			String query = "SELECT title,author,editor FROM `book` WHERE author = '" + author + "'";
			rs = stm.executeQuery(query);
			while (rs.next())
				System.out.println(rs.getString(1) + " witten by " + rs.getString(2) + " dans la bibliothèque : "
						+ rs.getString(3));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// recherche livre par ID
	public void searchBookId(int id) {
		try {

			String query = "SELECT title,author,editor FROM `book` WHERE id = " + id;
			rs = stm.executeQuery(query);
			while (rs.next())
				System.out.println(
						rs.getString(1) + " - " + rs.getString(2) + " dans la bibliothèque : " + rs.getString(3));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// tri livre par auteur
	public void sortBookAuthor() {
		try {
			String query = "SELECT title, author , lib_name FROM book JOIN library on book.library_id = library.id ORDER BY `book`.`author` ASC\n"
					+ "";
			rs = stm.executeQuery(query);
			while (rs.next())
				System.out.println(
						rs.getString(1) + " - " + rs.getString(2) + " dans la bibliothèque : " + rs.getString(3));
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	// afficher les livres de la bibliothèue
	public void listBookLibrary(String library) throws SQLException {

		String query = "SELECT title FROM book inner JOIN library on library.id = book.library_id where lib_name = '"
				+ library + "'";
		rs = stm.executeQuery(query);
		while (rs.next())
			System.out.println(rs.getString(1));

	}

	// rechercher utilisateur par id
	public void searchUserId(int id) {
		try {

			String query = "SELECT prenom,nom,addresse,numTel,mail FROM `user` WHERE id = " + id;
			rs = stm.executeQuery(query);
			while (rs.next())
				System.out.println(rs.getString(1) + " " + rs.getString(2) + " - " + rs.getString(3) + " - "
						+ rs.getString(4) + " - " + rs.getString(5));
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	// ajouter un livreà la bibliothèque
	public void addBook(Book book) {

		try {
			String query = "INSERT INTO book (id,title,author, editor,  page_nb, summary, library_id) VALUES ( '"
					+ book.getId() + "','" + book.getTitle() + "','" + book.getAuthor() + "' , '" + book.getEditor()
					+ "' , '" + book.getPageNb() + "' ,'" + book.getSummary() + "','" + book.getLibrary().getId() + "')";
			stm.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// afficher les livres en cours de prêt de la bibliothèque passée en paramètre;
	public void listBookRent(String library) throws SQLException {

		String query = "SELECT title FROM book inner JOIN library on library.id = book.library_id right JOIN rent on rent.book_id=book.library_id where lib_name = '"
				+ library + "'";
		rs = stm.executeQuery(query);
		while (rs.next())
			System.out.println(rs.getString(1));

	}

	/**
	**************************************
	**************************************
	**************************************
	**************************************
	**************************************
	**************************************
	**************************************
	**************************************
	*/
	// En cours de test

// afficher les livres en cours de prêt d'un utilisateur passé en paramètre;
	public void listBookRentUser(User user) {
		try {

			String query = "SELECT book.title from user join rent on rent.user_id = " + user.getId() + "'";
			rs = stm.executeQuery(query);
			while (rs.next())
				System.out.println(rs.getString(1));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// trier tous les prêts par date de fin décroissante d'une library passée en
	// paramètre;
	public void sortRentDateEnd(Library library) {

	}

	// créer un utilisateur;
	public void createUser(User user) {

		try {
			rs = stm.executeQuery("SELECT MAX(id) FROM `user`");
			rs.next();
			int nextId = rs.getInt(1) + 1;
			String query = "INSERT INTO `user`( `id`,`prenom`, `nom`, `addresse`, `numTel`, `mail`) " + "VALUES ("
					+ nextId + ",'" + user.getPrenom() + "','" + user.getNom() + "','" + user.getAddress() + "',"
					+ user.getNumTel() + ",'" + user.getMail() + "')";
			stm.executeUpdate(query);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// créer un prêt;
	public void createRent(Rent rent) throws SQLException {

		rs = stm.executeQuery("SELECT MAX(id) FROM `rent`");
		rs.next();
		int nextId = rs.getInt(1) + 1;

		String query = "INSERT INTO `rent`(`id`, `date_pret`, `date_fin`, `user_id`, `book_id`)" + "VALUES ( " + nextId
				+ " ,'" + rent.getDatePret() + "','" + rent.getDateFin() + "','" + rent.getUser().getId() + "','"
				+ rent.getBook().getId() + "')";
		stm.executeUpdate(query);

	}

	// créer un livre;

	public void createBook(Book book) throws SQLException {

		rs = stm.executeQuery("SELECT MAX(id) FROM `book`");
		rs.next();
		int nextId = rs.getInt(1) + 1;

		String query = "INSERT INTO `book`(`id`, `title`, `author`, `editor`, `summary`, `page_nb`, `library_id`)"
				+ " VALUES (" + nextId + ",'" + book.getTitle() + "','" + book.getAuthor() + "','" + book.getEditor()
				+ "','" + book.getSummary() + "'," + book.getPageNb() + "," + book.getLibrary() + ") ";
		stm.executeUpdate(query);

	}

}

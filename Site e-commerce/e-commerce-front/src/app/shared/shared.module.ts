import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './loader/loader.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule],
  declarations: [
    LoaderComponent,
  ],
  exports: [
    LoaderComponent,
    CommonModule,
    RouterModule,
  ],
  providers: [],
  entryComponents: []
})
export class SharedModule {}

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {

  constructor(private authenticationService: AuthenticationService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):  Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const url: string = state.url;

    return this.checkLogin(url);
  }
  checkLogin(url: string): boolean{
    console.log(url)
    let helper = new JwtHelperService();
    let token = localStorage.getItem('token');
    let decoded = helper.decodeToken(token);
    if (token != null) {
      if (decoded.role == 'ADMIN') {  // to change according to your back-end response
        this.router.navigate(['adminDashboard']); // to change according to your path
        return false;
      }
      if (decoded.role == 'BUYER') { // to change according to your back-end response
        this.router.navigate(['buyerDashboard']); // to change according to your path
        return false;
      }
    }
    return true;
  }
}

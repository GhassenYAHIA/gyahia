import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let helper = new JwtHelperService();
    let token = localStorage.getItem('token');
    let decoded = helper.decodeToken(token);
    if (token == null) {
      this.router.navigate(['login']);
      return false;
    }
   
    if (decoded.role == next.data.role) {
      return true;
    }
    if (decoded.role == 'MANAGER') {  // to change according to your back-end response
      this.router.navigate(['managerDashboard']); // to change according to your path
      return false;
    }
    if (decoded.role == 'COLLABORATOR') { // to change according to your back-end response
      this.router.navigate(['collaboratorDashboard']); // to change according to your path
      return false;
    }
    
  }

}

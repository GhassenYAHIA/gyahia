import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, BehaviorSubject, of } from "rxjs";
import { map } from "rxjs/operators";
import { User } from "../models/user";
import { Router } from "@angular/router";
import { JwtHelperService } from "@auth0/angular-jwt";
import { environment } from 'src/environments/environment';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: "root",
})
export class AuthenticationService {
  isLoadingResults = false;
  user: User;
  isLoggedIn = false;
  redirectUrl: string;
  currentUserSubject: BehaviorSubject<User>;
  currentUser: Observable<User>;

  constructor(private http: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem("currentUser"))
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  public setCurrentUser(user: User) {
    this.currentUserSubject.next(user);
  }

  login(email: string, password: string): any {
    return this.http.post<any>(apiUrl + `/login`, { email, password }, { observe: "response" });
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }

  logout() {
    localStorage.clear();
    this.isLoggedIn = false;
    this.currentUserSubject.next(null);
    this.router.navigate(["login"]);
  }

  register(
    email: string,
    password: string,
    firstName: string,
    lastName: string,
    username: string
  ): Observable<any> {
    return this.http
      .post<any>(apiUrl + `/users/signup`, {
        email,
        password,
        firstName,
        lastName,
        username,
      })
      .pipe(
        map((user) => {
          this.currentUserSubject.next(user);
          return user;
        })
      );
  }

  getCurrentUser(): Observable<any> {
    // decryter le token pour recupérer le username

    const token = localStorage.getItem("token");
    let helper = new JwtHelperService();
    const decoded = helper.decodeToken(token);
    const email = decoded.sub;
    return this.http.get<any>(apiUrl + `/users/` + email, {
      observe: "response",
    });
  }
}
export interface User {
    id: number;
    username: string;
    email: string;
    roles: string;
    token?: string;
    password?: string;
    firstName: string;
    lastName: string;
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import * as jwt_decode from "jwt-decode";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(private authenticationService: AuthenticationService, private router: Router) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const token = localStorage.getItem("token");
        if (token) {
            request = request.clone({
                setHeaders: {
                    Authorization: "Bearer " + token,
                    header: 'Access-Control-Allow-Headers : *'

                },
            });
        }

        return next.handle(request).pipe(tap(
            (err: any) => {
                if (err instanceof HttpErrorResponse) {
                    if (err.status === 401) {
                        this.router.navigate(['login']);
                    }

                    const exp = jwt_decode(token).exp;
                    if (exp < new Date().getTime() / 1000) {
                        alert("session expired !");
                        localStorage.removeItem("token");
                        this.router.navigate(["login"]);
                    }

                }
            }
        ));
    }

}

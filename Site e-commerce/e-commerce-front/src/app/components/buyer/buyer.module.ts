import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BuyerRoutingModule } from './buyer-routing.module';
import { BuyerDashboardComponent } from './buyer-dashboard/buyer-dashboard.component';

@NgModule({
  declarations: [BuyerDashboardComponent],
  imports: [
    CommonModule,
    BuyerRoutingModule
  ]
})
export class BuyerModule { }

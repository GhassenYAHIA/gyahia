import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from 'src/app/authentication/components/login/login.component';
import { BuyerDashboardComponent } from './buyer-dashboard/buyer-dashboard.component';
import { HomeComponent } from '../home/home.component';


const routes: Routes = [
  { path: '', component: HomeComponent, data: { title: 'Home' } },
  { path: 'login', component: LoginComponent },
  {
    path: 'BuyerDashboard',
    component: BuyerDashboardComponent, 
    data: { title: 'buyerDashboard', role: 'BUYER' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyerRoutingModule { }

import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthenticationService } from "src/app/core/services/authentication.service";
import { Router } from "@angular/router";
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  isLoadingResults = true;
  isLoggedIn = false;

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email, Validators.pattern]],
      password: ["", [Validators.required, Validators.minLength(6)]],
    });


  }


  get email() {
    return this.loginForm.get("email");
  }

  get password() {
    return this.loginForm.get("password");
  }

  login() {
    const email = this.loginForm.value.email;
    const password = this.loginForm.value.password;

    this.authenticationService.login(email, password).subscribe(
      (res) => {

        if (res.status == 200) {
          this.isLoggedIn = true;
          const token = res.headers.get("Authorization");
          let helper = new JwtHelperService();
          let decoder = helper.decodeToken(token);
          this.authenticationService.setCurrentUser(helper.decodeToken(token));
          let role = decoder.role;
          let id = decoder.id;
          localStorage.setItem("role", role);
          localStorage.setItem("token", token);
          localStorage.setItem("id", id);
          //ROUTING
          switch (role) {
            case "BUYER": {
              this.router.navigateByUrl("/buyerDashboard");
              break;
            }
            case "ADMIN": {
              this.router.navigateByUrl("/adminDashboard");
              break;
            }
          }
        }
      },
      (err) => { this.isLoadingResults = false; }
    );
  }
}

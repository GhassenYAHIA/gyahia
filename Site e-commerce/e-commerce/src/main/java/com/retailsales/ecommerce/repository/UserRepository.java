package com.retailsales.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.retailsales.ecommerce.entity.User;

@Repository
public interface UserRepository extends  JpaRepository<User, Integer>, JpaSpecificationExecutor<User>{

	User findByEmail(String email);

}

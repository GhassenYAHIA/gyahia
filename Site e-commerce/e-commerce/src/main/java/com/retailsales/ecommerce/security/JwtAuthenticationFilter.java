package com.retailsales.ecommerce.security;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.retailsales.ecommerce.entity.User;
import com.retailsales.ecommerce.model.UserPrincipal;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private AuthenticationManager authenticationManager;

	public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	// called when login
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {

		// Grab credentials and map them
		User credentials = null;
		try {
			credentials = new ObjectMapper().readValue(request.getInputStream(), User.class);
		} catch (IOException e) {
			logger.fatal("Unable to find credentials.", e);

		}
		
		if(credentials == null) {
			return null;
		}		
		

		// Create login token

		
			UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
					credentials.getEmail(), credentials.getPassword(), new ArrayList<>());

			// Authenticate user
			return authenticationManager.authenticate(authenticationToken);
		

	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		// Grab principal
		UserPrincipal principal = (UserPrincipal) authResult.getPrincipal();

		// Create JWT Token
		String token = JWT.create().withSubject(principal.getEmail())
				.withExpiresAt(new Date(System.currentTimeMillis() + JwtProperties.EXPIRATION_TIME))
				.withClaim("role", principal.getRole()).withClaim("id", principal.getId())
				.sign(HMAC512(JwtProperties.SECRET.getBytes()));

		// Add token in response
		response.addHeader(JwtProperties.HEADER_STRING, JwtProperties.TOKEN_PREFIX + token);

	}
}

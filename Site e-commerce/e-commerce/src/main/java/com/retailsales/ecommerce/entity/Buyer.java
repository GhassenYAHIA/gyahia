package com.retailsales.ecommerce.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.retailsales.ecommerce.util.Roles;

@Entity
@Table(name = "buyer")
public class Buyer extends User {
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinTable(name = "buyer_ad", joinColumns = {
			@JoinColumn(name = "buyer_id", referencedColumnName = "id", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "ad_id", referencedColumnName = "id", nullable = false, updatable = false) })
	private List<Ad> ads= new ArrayList<>();
	public Buyer() {
		super();
		setRole(Roles.BUYER);
	}
}

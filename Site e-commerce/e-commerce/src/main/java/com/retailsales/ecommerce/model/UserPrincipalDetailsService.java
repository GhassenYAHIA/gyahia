package com.retailsales.ecommerce.model;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.retailsales.ecommerce.entity.User;
import com.retailsales.ecommerce.repository.UserRepository;

@Service
public class UserPrincipalDetailsService implements UserDetailsService {
	private UserRepository userRepository;

	public UserPrincipalDetailsService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String s)  {
		User user = this.userRepository.findByEmail(s);
		return new UserPrincipal(user);
		
	}
}

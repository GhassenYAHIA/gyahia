package com.retailsales.ecommerce.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.retailsales.ecommerce.util.Roles;

@Entity
@Table(name = "admin")
public class Admin extends User {
	@OneToMany(mappedBy="admin", cascade = CascadeType.ALL)
	private List<Ad> adsList;

	public Admin() {
		super();
		setRole(Roles.ADMIN);
	}
}

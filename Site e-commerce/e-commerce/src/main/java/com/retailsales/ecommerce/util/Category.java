package com.retailsales.ecommerce.util;

public enum Category {
	SHIRT, PANTS, JACKET
}

package com.thp.project.vintud.dao.impl;

import java.sql.Connection;

import com.thp.project.vintud.dao.factory.FactoryDao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.thp.project.vintud.dao.IAdDao;
import com.thp.project.vintud.entity.Ad;
import com.thp.project.vintud.entity.Category;


public class AdDaoImpl implements IAdDao {
	
	private FactoryDao factoryDao;

	public AdDaoImpl(FactoryDao factoryDao) {
		this.factoryDao = factoryDao;
	}
	
	public int create(Ad newAd, int userID, int categoryId) throws SQLException {
	Connection connect=FactoryDao.getConnection();
	Statement stm = null;
	int lastKey = 1;
	try {
		String query = "INSERT INTO announcement (title ,description,price , picture,publication_date,is_available,view_number,localisation,category_id,user_id) "
				+ "VALUES ('" + newAd.getTitleAd() + "','" + newAd.getDescriptionAd() + "'," + newAd.getPriceAd()
				+ ",'" + newAd.getPictureAd() + "','" + newAd.getDatePublication() + "'," + newAd.isAvailabilityAd()
				+ "," + newAd.getNumberView() + ",'" + newAd.getLocalisationAd() + "',"
				+ categoryId + "," + userID + ")";
		stm = connect.createStatement();
		stm.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
		System.out.println("create ok");
		ResultSet keys = stm.getGeneratedKeys();

		while (keys.next()) {
			lastKey = keys.getInt(1);
		}

		newAd.setIdAd(lastKey);
		stm.close();

		
	} catch (SQLException e) {
		e.printStackTrace();

	}
	return lastKey;
}

	public int update(Ad announce) throws SQLException {
	Connection connect=FactoryDao.getConnection();
	Statement stm = null;

		String query = "Update announcement  SET title ='" + announce.getTitleAd() + "', description='"
				+ announce.getDescriptionAd() + "', price=" + announce.getPriceAd() + ", picture='"
				+ announce.getPictureAd() + "' , publication_date = '" + announce.getDatePublication()
				+ "' , view_number =" + announce.getNumberView() + ",localisation = '"
				+ announce.getLocalisationAd() +"',is_available="+ announce.isAvailabilityAd()+" , category_id = " + announce.getCategoryAd().getIdCategory()
				+ ",user_id =" + announce.getUserAd().getIdUser() + " WHERE id=" + announce.getIdAd() + ";";

		int  key = connect.createStatement().executeUpdate(query);
		
		stm.close();

		return key;

}

	public int delete(int idTodelete) throws SQLException {
	Connection connect=FactoryDao.getConnection();
	PreparedStatement stm = null;

		String query = "DELETE FROM announcement WHERE id =" + idTodelete;
		
		return connect.createStatement().executeUpdate(query);
	
}
	
	public List<Ad> getAvailableAds() {
		Connection connect=FactoryDao.getConnection();

	    List<Ad> announcements=  new ArrayList<Ad>();
		try {
			String sqlQuery = "SELECT * FROM `announcement` ";
			
			ResultSet resultSet = connect.createStatement().executeQuery(sqlQuery);
			Ad ads = null;
			while (resultSet.next()) {
				 ads =new Ad();
				 ads.setTitleAd(resultSet.getString("title"));
				 ads.setDescriptionAd(resultSet.getString("description"));
				 ads.setPriceAd(resultSet.getInt("price"));
				 ads.setLocalisationAd(resultSet.getString("localisation"));
				 ads.setAvailabilityAd(true);
				 ads.setIdAd(resultSet.getInt("id"));
			announcements.add(ads);
			}
			resultSet.close();

		} catch (SQLException exception) {
			exception.printStackTrace();
		}
		return announcements;

	}
	
	public List<Ad> filterAdsByPrice(double min, double max) {
	Connection connect=FactoryDao.getConnection();

	 
	    List<Ad> announcements=  new ArrayList<Ad>();

		try {
		String sqlQuery = "SELECT * FROM `announcement`where price BETWEEN " + min + " AND " +max ;
		ResultSet resultSet = connect.createStatement().executeQuery(sqlQuery);
		    System.out.println("the announcement where price is between" + min +" and "+ max + " are : ");
			Ad ads = null;

		    while (resultSet.next()) {
			 ads =new Ad();
			 ads.setTitleAd(resultSet.getString("title"));
			 ads.setDescriptionAd(resultSet.getString("description"));
			 ads.setPriceAd(resultSet.getInt("price"));
			 ads.setLocalisationAd(resultSet.getString("localisation"));
			 ads.setAvailabilityAd(true);
			 ads.setIdAd(resultSet.getInt("id"));
		announcements.add(ads);
		}
		resultSet.close();

		} catch (SQLException exception) {
			exception.printStackTrace();
		}
		return announcements;

	}
	 
	public List<Ad> getAdsByCategory(String category){
	    	ResultSet rs = null;
	    	Statement stmt = null;
		Connection connect=FactoryDao.getConnection();
		List<Ad> announcements=  new ArrayList<Ad>();

			try {				
			String query = "SELECT * FROM announcement JOIN category "
					+ "ON category.id = announcement.category_id "
					+ "where category.name= '"+category+"'" ;
			ResultSet resultSet = connect.createStatement().executeQuery(query);

			System.out.println("the announecemnt in " + category + " category are : ");
			Ad ads = null;

			    while (resultSet.next()) {
				 ads =new Ad();
				 ads.setTitleAd(resultSet.getString("title"));
				 ads.setDescriptionAd(resultSet.getString("description"));
				 ads.setPriceAd(resultSet.getInt("price"));
				 ads.setLocalisationAd(resultSet.getString("localisation"));
				 ads.setAvailabilityAd(true);
				 ads.setIdAd(resultSet.getInt("id"));
			announcements.add(ads);
			}
			resultSet.close();

			} catch (SQLException exception) {
				exception.printStackTrace();
			}
			return announcements;
	}

	public List<Ad> filterAdsLocation(String location) {

		Connection connect=FactoryDao.getConnection();
		List<Ad> announcements=  new ArrayList<Ad>();

			try {				
			String query = "SELECT * FROM `announcement`where localisation LIKE '%" + location + "%'";
			ResultSet resultSet = connect.createStatement().executeQuery(query);

			System.out.println("les announcement located in " + location + " are : ");
			Ad ads = null;

			    while (resultSet.next()) {
				 ads =new Ad();
				 ads.setTitleAd(resultSet.getString("title"));
				 ads.setDescriptionAd(resultSet.getString("description"));
				 ads.setPriceAd(resultSet.getInt("price"));
				 ads.setLocalisationAd(resultSet.getString("localisation"));
				 ads.setAvailabilityAd(true);
				 ads.setIdAd(resultSet.getInt("id"));
			announcements.add(ads);
			}
			resultSet.close();

			} catch (SQLException exception) {
				exception.printStackTrace();
			}
			return announcements;
	}
	
}


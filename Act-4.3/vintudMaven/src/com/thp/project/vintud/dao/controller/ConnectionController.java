package com.thp.project.vintud.dao.controller;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.thp.project.vintud.dao.factory.FactoryDao;
import com.thp.project.vintud.dao.impl.UserDaoImpl;
import com.thp.project.vintud.entity.User;
import com.thp.project.vintud.entity.impl.SignUpForm;


@WebServlet("/ConnectionController")
public class ConnectionController extends HttpServlet {

	final FactoryDao factoryDao = new FactoryDao();

	public ConnectionController() {
		super();

	}

	public static final String VUE_SUCCES = "/WEB-INF/Home.jsp";
	public static final String VUE_FORM = "/WEB-INF/Connection.jsp";

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher("/WEB-INF/Connection.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String email = request.getParameter("email");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String pseudo = request.getParameter("pseudo");
		String password = request.getParameter("password");
		String phone = request.getParameter("phone");
		String adress = request.getParameter("adress");

		UserDaoImpl user = factoryDao.getUserDao();
		User newUser = new User();
		newUser.setMail(email);
		newUser.setFirstName(firstName);
		newUser.setLastName(lastName);
		newUser.setPassword(password);
		newUser.setPhone(Integer.parseInt(phone));
		newUser.setPseudo(pseudo);
		newUser.setAddress(adress);
		newUser.setRoleId(1);

		/* Pr�paration de l'objet formulaire */
		SignUpForm form = new SignUpForm();

		/* Traitement de la requ�te et r�cup�ration du bean en r�sultant */
		User client = form.creerClient(request);

		/* Ajout du bean et de l'objet m�tier � l'objet requ�te */

		request.setAttribute("form", form);
		request.setAttribute("user", newUser);

		if (form.getErreurs().isEmpty()) {
			/* Si aucune erreur, alors affichage de la fiche r�capitulative */
			try {
				user.create(newUser);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			this.getServletContext().getRequestDispatcher(VUE_SUCCES).forward(request, response);

		} else {
			this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
		}

	}

}

package com.thp.project.vintud.dao;

import java.sql.SQLException;

import com.thp.project.vintud.entity.Roles;
import com.thp.project.vintud.entity.User;

public interface IUserDao{

	public String getViews(int idAd) throws SQLException;


	public int create(User user) throws SQLException;

	public int update(User user) throws SQLException;
	
	public String getBuyerInformation(User user) throws SQLException;
	

	
	public int updateRole(User user, Roles role) throws SQLException;
	
	
	
	public boolean login(String mail, String password) throws SQLException;
	
}

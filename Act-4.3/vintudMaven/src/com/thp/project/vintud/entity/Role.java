package com.thp.project.vintud.entity;

public class Role {

	private static  Roles nameRole;
	private int idRole;

	public Role() {
	}

	public Role(Roles nameRole) {
		Role.nameRole = nameRole;
	}

	public Roles getNameRole() {
		return nameRole;
	}

	public int getIdRole() {
		return idRole;
	}

	public void setNameRole(Roles nameRole) {
		Role.nameRole = nameRole;
	}

	public void setIdRole(int idRole) {
		this.idRole = idRole;
	}

}

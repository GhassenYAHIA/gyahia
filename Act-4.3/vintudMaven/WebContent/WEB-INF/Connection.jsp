<%@taglib uri= "http://java.sun.com/jsp/jstl/core" prefix= "c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<style><%@include file="/../css/custom.css"%></style>
<body>
 
<header>
<jsp:include page="header.html"></jsp:include>
</header>	
			<div id="login">
    <h3 class="text-center text-white pt-5">Inscription form</h3>
  
    <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6 login">
                <div class="login-box col-md-12">
                    <form id="login-form" class="form" action="SignUp" method="Post">
                        <h3 class="text-center text-info">Register</h3>
                        <div class="form-group">
                            <label for="firstName" class="text-info">FirstName:</label><br>
                            <input type="text" name="firstName" id="firstName" value="<c:out value="${user.firstName}"/>" class="form-control" required>
                        </div>
                         <div class="form-group">
                            <label for="Name" class="text-info">Name:</label><br>
                            <input type="text" name="lastName" id="name" value="<c:out value="${user.lastName}"/>" class="form-control" required>
                        </div>
                         <div class="form-group">
                            <label for="pseudo" class="text-info">Pseudo:</label><br>
                            <input type="text" name="pseudo" id="pseudo"   value="<c:out value="${user.pseudo}"/>" class="form-control" required>
                        </div>
                         <div class="form-group">
                            <label for="email" class="text-info">Email:</label><br>
                            <input type="text" name="email" id="email" value="<c:out value="${user.mail}"/>" class="form-control" required>
                           <span class="erreur">${form.erreurs['email']}</span>
                        </div>
                        <div class="form-group">
                            <label for="password" class="text-info">Password:</label><br>
                            <input type="password" name="password" id="password" value="<c:out value="${user.password}"/>"  class="form-control" required>
                        </div>
                          <div class="form-group">
                            <label for="phone" class="text-info">Phone:</label><br>
                            <input type="number" name="phone" id="phone" value="<c:out value="${user.phone}"/>" class="form-control" required>
                        <span class="erreur">${form.erreurs['phone']}</span>
                        </div>
                           <div class="form-group">
                            <label for="adress" class="text-info">Adress:</label><br>
                            <input type="text" name="adress" id="adress"  value="<c:out value="${user.address}"/>"  class="form-control" required>
                        </div>
                        <p class="info">${ form.resultat }</p>
                        <div class="form-group">
                            <label for="remember-me" class="text-info"> </label><br>
                            <input type="submit" name="submit" class="btn btn-info btn-md" value="SignUp">
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
	


</body>

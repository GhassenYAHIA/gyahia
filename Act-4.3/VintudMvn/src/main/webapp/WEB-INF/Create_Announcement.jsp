<%@taglib uri= "http://java.sun.com/jsp/jstl/core" prefix= "c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style><%@include file="/../css/custom.css"%></style>
<body>
 
<header>
<jsp:include page="header.html"></jsp:include>
</header>
	
	
			<div id="login">
    <h3 class="text-center text-white pt-5">create Announce form</h3>
  
    <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6 login">
                <div class="login-box col-md-12">
                    <form id="login-form" class="form" action="CreateAnnouncement" method="Post">
                        <h4 class="text-center text-info">Deposer une annonce</h4>
                        <div class="form-group">
                            <label for="title" class="text-info">Title:</label><br>
                            <input type="text" name="title" id="title" class="form-control" required>
                        </div>
                         <div class="form-group">
                            <label for="description" class="text-info">Description:</label><br>
                            <textarea name="description" rows="4" id="description" class="form-control"></textarea>
                        </div>
                         <div class="form-group">
                            <label for="price" class="text-info">Price:</label><br>
                            <input type="number" name="price" id="price" class="form-control" min=0 required>
                        </div>
                         <div class="form-group">
                            <label for="category" class="text-info">Category:</label><br>
                            <select  name="category"  class="form-control">
                            <option value="T-shirt">T-shirt</option>
			                   	<option value="Polo">Polo</option>
			                	<option value="veste">Veste</option>
			                	<option value="pantalon">Pantalon</option>
			                     </select>
                        </div>
                         <div class="form-group">
                            <label for="localisation" class="text-info">Localisation:</label><br>
                            <input type="localisation" name="localisation" id="localisation" class="form-control" required>
                        </div>
                        
                        <div class="form-group">
                            <label for="remember-me" class="text-info"> </label><br>
                            <input type="submit" name="submit" class="btn btn-info btn-md" value="Add ">
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
	


</body>

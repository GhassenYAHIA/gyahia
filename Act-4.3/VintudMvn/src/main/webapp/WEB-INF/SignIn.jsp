<%@taglib uri= "http://java.sun.com/jsp/jstl/core" prefix= "c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<style><%@include file="/../css/custom.css"%></style>



<jsp:include page="header.html"></jsp:include>
			<div id="login">
    <h3 class="text-center text-white pt-5">Login form</h3>
  
    <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6 login">
                <div class="login-box col-md-12">
                    <form id="login-form" class="form" action="SignIn" method="Post">
                        <h3 class="text-center text-info">Login</h3>
                        
                        <p class="info" style="color:red">${ form.resultat }</p>
                        <div class="form-group">
                            <label for="email" class="text-info">Email *:</label><br> 
                            <input type="text" name="email" id="email" class="form-control" required>
                        
                        </div>
                        <div class="form-group">
                            <label for="password" class="text-info">Password *:</label><br>
                            <input type="text" name="password" id="password" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="remember-me" class="text-info"> </label><br>
                            <input type="submit" name="submit" class="btn btn-info btn-md" value="submit">
                        </div>
                        <div id="register-link" class="text-right">
                            <a href="./SignUp" class="text-info">Register here</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
	

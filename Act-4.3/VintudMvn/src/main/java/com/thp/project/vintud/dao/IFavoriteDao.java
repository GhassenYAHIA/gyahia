package com.thp.project.vintud.dao;


import java.sql.SQLException;
import java.util.List;

import com.thp.project.vintud.entity.Favorite;

public interface IFavoriteDao {

	int insertFavorite(Favorite entity, int userId, int announceId) throws SQLException;

	int delete(int favoriteId) throws SQLException;

	List<Favorite> findAnnounce(int idAnnounce) throws SQLException;

	int update(Favorite fav) throws SQLException;

}
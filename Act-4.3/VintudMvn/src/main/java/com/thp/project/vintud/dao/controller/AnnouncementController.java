package com.thp.project.vintud.dao.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thp.project.vintud.dao.factory.FactoryDao;
import com.thp.project.vintud.dao.impl.AdDaoImpl;
import com.thp.project.vintud.entity.Ad;



@WebServlet("/AnnouncementController")
public class AnnouncementController extends HttpServlet {
	
	final FactoryDao factoryDao = new FactoryDao();
       
    public AnnouncementController() {
        super();
    }
    
    
    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		AdDaoImpl adImpl= factoryDao.getAdDao();
		List<Ad> announcementsList = adImpl.getAvailableAds();
		request.setAttribute("announcements", announcementsList);
		this.getServletContext().getRequestDispatcher("/WEB-INF/Announcement.jsp").forward(request, response);
	}

	

}

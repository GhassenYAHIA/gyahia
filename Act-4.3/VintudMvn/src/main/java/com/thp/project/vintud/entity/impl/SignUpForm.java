package com.thp.project.vintud.entity.impl;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import com.thp.project.vintud.dao.factory.FactoryDao;
import com.thp.project.vintud.dao.impl.UserDaoImpl;
import com.thp.project.vintud.entity.User;

public class SignUpForm {

	private static final String CHAMP_EMAIL = "email";
	private static final String CHAMP_PHONE = "phone";
	private String resultat;
	private Map<String, String> erreurs = new HashMap<String, String>();
	final FactoryDao factoryDao = new FactoryDao();

	public Map<String, String> getErreurs() {
		return erreurs;
	}

	public String getResultat() {
		return resultat;
	}

	public User creerClient(HttpServletRequest request) {

		User client = new User();
		String email = getValeurChamp(request, CHAMP_EMAIL);
		String phone = getValeurChamp(request, CHAMP_PHONE);
		try {
			validationEmail(email);
		} catch (Exception e) {
			setErreur(CHAMP_EMAIL, e.getMessage());
		}
		try {
			validationtel(phone);
		} catch (Exception e) {
			setErreur(CHAMP_PHONE, e.getMessage());
		}
		client.setMail(email);

		if (erreurs.isEmpty()) {
			resultat = "Succ�s de la cr�ation du client.";
		} else {
			resultat = "�chec de la cr�ation du client.";
		}

		return client;

	}

	private void validationEmail(String email) throws Exception {
		UserDaoImpl user = factoryDao.getUserDao();
		if (email != null && !email.matches("([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)")) {
			throw new Exception("Merci de saisir une adresse mail valide.");
		} else if (user.findUserByEMail(email)) {
			throw new Exception("Ce mail exite d�j�!");
		}

	}
	
	private void validationtel(String tel) throws Exception {
		UserDaoImpl user = factoryDao.getUserDao();
		if (user.findUserByTEL(tel)) {
			throw new Exception("Ce t�l�phone exite d�j�!");
		}

	}

	/*
	 * Ajoute un message correspondant au champ sp�cifi� � la map des erreurs.
	 */
	private void setErreur(String champ, String message) {
		erreurs.put(champ, message);
	}

	/*
	 * M�thode utilitaire qui retourne null si un champ est vide, et son contenu
	 * sinon.
	 */
	private static String getValeurChamp(HttpServletRequest request, String nomChamp) {
		String valeur = request.getParameter(nomChamp);
		if (valeur == null || valeur.trim().length() == 0) {
			return null;
		} else {
			return valeur;
		}
	}

}

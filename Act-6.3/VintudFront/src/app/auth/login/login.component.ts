import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  username = '';
  password = '';
  isLoadingResults = false;
  token: any;

  constructor(private formBuilder: FormBuilder, private router: Router, private authService:
    AuthService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      'username': [null, Validators.required],
      'password': [null, Validators.required]
    });
  }

  onFormSubmit(form: NgForm) {
    this.authService.login(form)
      .subscribe((res: Response) => {
        console.log(res);
        if (res) {
          localStorage.setItem('token', res.headers.get('Authorization'));
          this.router.navigate(['ads']);
        }
      }, (err) => {
        console.log(err);
      });
  }




  register() {
    this.router.navigate(['register']);
  }

}

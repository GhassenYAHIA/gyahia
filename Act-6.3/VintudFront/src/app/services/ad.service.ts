import { Injectable } from '@angular/core';
import { Ad } from '../models/ad.model';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class AdService {

  

  adsSubject = new Subject<Ad[]>()

  ads: Ad[];

  emitAdSubject() {
    this.adsSubject.next(this.ads);
  }


  createAd(ad: Ad) {
    this.ads.push(ad);
  }

  deleteAd(ad: Ad) {
    this.ads.splice(this.ads.findIndex(obj => obj === ad), 1);
    this.emitAdSubject()
  }

  constructor(private http: HttpClient) { }

  getAds(): Observable<Ad[]> {
    return this.http.get<Ad[]>(apiUrl + 'ads')
      .pipe(
        tap(_ => this.log('fetched Ads')),
        catchError(this.handleError('getAds', []))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }
}


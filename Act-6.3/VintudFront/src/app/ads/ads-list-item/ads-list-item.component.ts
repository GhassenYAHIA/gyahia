import { Component, OnInit, Input } from '@angular/core';
import { Ad } from '../../models/ad.model';
import { AdService } from '../../services/ad.service';

@Component({
  selector: 'app-ads-list-item',
  templateUrl: './ads-list-item.component.html',
  styleUrls: ['./ads-list-item.component.scss']
})
export class AdsListItemComponent implements OnInit {
  @Input() ad: Ad;

  constructor(private adService: AdService) { }

  ngOnInit() {
  }


}

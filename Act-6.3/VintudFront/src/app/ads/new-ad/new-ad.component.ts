import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AdService } from 'src/app/services/ad.service';
import { Ad } from 'src/app/models/ad.model';

@Component({
  selector: 'app-new-ad',
  templateUrl: './new-ad.component.html',
  styleUrls: ['./new-ad.component.scss']
})
export class NewAdComponent implements OnInit {

  adForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private adService: AdService) { }

  ngOnInit() {
    this.initForm()
  }

  initForm() {
    this.adForm = this.formBuilder.group({
      titleAd: ['', Validators.required],
      pictureAd:[''],
      descriptionAd: ['', Validators.required],
      priceAd : ['', Validators.required],
      datePublication : ['', Validators.required],
      localisationAd :['', Validators.required],
    })
  }

  onSubmitForm() {
    const title = this.adForm.get('title').value
    const content = this.adForm.get('content').value
    const newAd = new Ad();
    this.adService.createAd(newAd)
    this.router.navigate(['/ads'])
  }
}

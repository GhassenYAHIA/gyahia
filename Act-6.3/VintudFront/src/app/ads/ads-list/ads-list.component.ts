import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs'

import { AdService } from '../../services/ad.service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { Ad } from '../../models/ad.model';

@Component({
  selector: 'app-ads-list',
  templateUrl: './ads-list.component.html',
  styleUrls: ['./ads-list.component.scss']
})
export class AdsListComponent implements OnInit {

  data: Ad[] = [];
  displayedColumns: string[] = ['title', 'description', 'price'];
  isLoadingResults = true;


  constructor(private adService: AdService, private authService: AuthService, private router: Router) { }

  adSubscription: Subscription;

  ngOnInit() {
  /*  this.adSubscription = this.adService.adsSubject.subscribe(
      (ads: Ad[]) => {
        this.ads = ads;
      }
    )
    this.adService.emitAdSubject();*/
    this.getAds();
  }

  ngOnDestroy() {
    this.adSubscription.unsubscribe();
  }
  
  getAds(): void {
    this.adService.getAds()
      .subscribe(ads => {
        this.data = ads;
        console.log(this.data);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }


  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }


}

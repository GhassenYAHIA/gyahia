import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './auth/auth.guard';
import { AdsListComponent } from './ads/ads-list/ads-list.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';

const routes: Routes = [{
  path: 'ads',
  canActivate: [AuthGuard],
  component: AdsListComponent,
  data: { title: 'List of Products' }
  },
  {
  path: 'login',
  component: LoginComponent,
  data: { title: 'Login' }
  },
  {
  path: 'register',
  component: RegisterComponent,
  data: { title: 'Register' }
  }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

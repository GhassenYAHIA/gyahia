export class User {
    public id: number;
    public firstName: string;
    public lastName: string;
    public pseudo: string;
    public upassword: string;
    public mail: string;
    public address: string;
    public phone: number;
    public roleId: number;
}
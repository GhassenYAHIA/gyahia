export class Ad {
    idAd    : number;
    titleAd : string;
    pictureAd  : string;
    descriptionAd : string;
    priceAd : number;
    datePublication : number;
    availabilityAd :boolean;
    localisationAd : string;
    numberView : number;
    idowner : string;
    category : string;
}

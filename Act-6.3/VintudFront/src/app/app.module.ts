import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdsListComponent } from './ads/ads-list/ads-list.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { HeaderComponent } from './header/header.component';
import { NewAdComponent } from './ads/new-ad/new-ad.component';
import { AdsListItemComponent } from './ads/ads-list-item/ads-list-item.component';


const appRoutes: Routes = [
  { path: 'ads', component: AdsListComponent, data: { title: 'List of Ads' } },
  { path: 'login', component: LoginComponent, data: { title: 'Login' } },
  { path: 'register', component: RegisterComponent, data: { title: 'Register' } },
  { path: 'new', component: NewAdComponent },
  { path: '', component: LoginComponent },
  { path: '**', redirectTo: 'ads' },

]

@NgModule({
  declarations: [
    AppComponent,
    AdsListComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    NewAdComponent,
    AdsListItemComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

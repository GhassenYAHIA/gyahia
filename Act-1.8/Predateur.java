// les enumerations
enum Sexe {
	MALE, FEMELLE;
}

enum Age {
	JEUNE, ADULTE, VIEUX;
}

enum Groupe {
	GROUPE, SOLITAIRE;
}

//declaration de la classe
public abstract class Predateur {

	protected Sexe sexe;
	protected Age age;
	protected String force;
	protected Groupe groupe;

	// constructeur
	public Predateur(Sexe sexe, Age age, String force, Groupe groupe) {
		this.sexe = sexe;
		this.age = age;
		this.force = force;
		this.groupe = groupe;
	}

	// accesseurs et mutateurs

	public String getSexe() {
		String input = this.sexe.toString();
		return input.substring(0, 1) + input.substring(1).toLowerCase(); // pour rendre l'entrée de enum avec une
																			// majuscule seulement pour lapremière
																			// lettre

	}

	public void setSexe(Sexe sexe) {
		this.sexe = sexe;
	}

	public String getAge() {
		String input = this.age.toString();
		return input.substring(0, 1) + input.substring(1).toLowerCase();
	}

	public void setAge(Age age) {
		this.age = age;

	}

	public String getForce() {
		return this.force;
	}

	public void setForce(String force) {
		this.force = force;
	}

	public String getGroupe() {
		String input = this.groupe.toString();
		return input.substring(0, 1) + input.substring(1).toLowerCase();
	}

	public void setGroupe(Groupe groupe) {
		this.groupe = groupe;
	}

}

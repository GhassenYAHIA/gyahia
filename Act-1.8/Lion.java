public class Lion extends Predateur implements PredateurAction, LionAction {

	private boolean facteurDomination;
	private String rang;
	private String facteurImpetuosite;
	private boolean dort;
	private boolean malade;
	private boolean statutGroupe = true;

	public Lion(Sexe sexe, Age age, String force,Groupe groupe,  boolean facteurDomination,
			String facteurImpetuosite) {
		super(sexe, age, force, groupe);
		this.facteurDomination = facteurDomination;
		this.facteurImpetuosite = facteurImpetuosite;

	}
	
	//accesseurs et mutateurs propres à Lion
	
	public boolean getFacteurDomination() {
		return facteurDomination;
	}

	public void setFacteurDomination(boolean facteurDomination) {
		this.facteurDomination = facteurDomination;
	}

	//le rang dépend de la domination
	
	public String getRang() {
		if (this.facteurDomination) {
			return this.rang;
		} else {
			return "Ce lion n'est pas dominant ! ";
		}
	}

	public void setRang(String rang) {
		this.rang = rang;

	}

	public String getFacteurImpetuosite() {
		return facteurImpetuosite;
	}

	public void setFacteurImpetuosite(String facteurImpetuosite) {
		this.facteurImpetuosite = facteurImpetuosite;
	}

	// implementations des methodes de l'interface predateur 
	
	public void seNourrir() {
		System.out.println("Le lion se nourrit uniquement de viande.");
	}

	public void chasser() {
		System.out.println("Le lion chasse en groupe.");
	}

	public void courrir() {
		System.out.println("Il a une vitesse  aux alentours de 50km/h");
	}

	public void seReproduire() {
		System.out.println("Le lion se reproduit toute l'année.");
	}

	public void emettreSon() {
		if (this.statutGroupe) {
			System.out.println("Le lion rugit pour communiquer avec sa troupe.");
		} else {
			System.out.println("Le lion étant seul, il ne rugira pas.");
		}
	}
	
	// methodes propres à la classe lion
	
	// entendre son dépend de l'état de lion, dort, réveillé, malade ou sain
	public void entendreSon() {
		if (!this.malade  && !this.dort) {
			System.out.println("Il entend très bien.");

		} else {
			System.out.println("Il n'entend pas.");
		}

	}

	public void dormir() {
		this.dort = true;
	}

	public void seReveiller() {
		this.dort = false;
	}

	public void malade() {
		this.malade = true;

	}

	public void sain() {
		this.malade = false;

	}
	
	
	/** méthode se séparer dépend de l'action du lion, part ou retourne au groupe
	cela change aussi la valeur de groupe et d'entendreson
	*/
	
	public void seSeparer() {
		if (this.statutGroupe) {
			System.out.println("Le lion est avec sa troupe.");
		} else {
			this.groupe=Groupe.SOLITAIRE;
			System.out.println("Le lion s'est séparé de sa troupe.");
		}
	}

	public void partirGroupe() {
		this.statutGroupe = false;
	}

	public void retournerGroupe() {
		this.statutGroupe = true;
	}

	//affichage des caractéristiques de lion communes aux prédateurs et qui lui sont propres
	public String toString() {
		return "Ce lion est " + this.getAge() + ", de sexe " + this.getSexe() + ", vit en " + this.getGroupe()
				+ ", a une force de " + this.getForce() + ",  un facteur de domination " + this.facteurDomination
				+ ", un rang  " + this.rang + " et un facteur d'impétuosité " + this.facteurImpetuosite + ".\n";
	}

	
}

package com.thp.project.vintud.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.thp.project.vintud.entity.Favorite;

public interface FavoriteRepository extends JpaRepository<Favorite, Integer> {

	List<Favorite> findByFavouriteUser(int iduser);

}

package com.thp.project.vintud.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name = "favorite")
public class Favorite implements Serializable {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int idFavourite;
	
	
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User favouriteUser;
	
	
	@ManyToOne
	@JoinColumn(name = "ad_id")
	private Ad favouriteAd;
	
	@Column(name = "favorite_date")
	private Timestamp favouriteDate;


	public Favorite(Timestamp favouriteDate) {

		this.favouriteDate = favouriteDate;
		
	}

	public Favorite() {
	}

	// getter
	public Timestamp getFavouriteDate() {
		return favouriteDate;
	}

	public User getFavouriteUser() {
		return favouriteUser;
	}

	public Ad getFavouriteAd() {
		return favouriteAd;
	}

	public int getIdFavourite() {
		return idFavourite;
	}

	// setter
	public void setFavouriteDate(Timestamp favouriteDate) {
		this.favouriteDate = favouriteDate;
	}

	public void setFavouriteUser(User favouriteUser) {
		this.favouriteUser = favouriteUser;
	}

	public void setFavouriteAd(Ad favouriteAd) {
		this.favouriteAd = favouriteAd;
	}

	public void setIdFavourite(int idFavourite) {
		this.idFavourite = idFavourite;
	}
	
	//cette methode permet d'afficher
	@Override
	public String toString() {
		return "Favorite [idFavourite=" + idFavourite + ", favouriteDate=" + favouriteDate + "]";
	}
	
	
	
}


package com.thp.project.vintud.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.thp.project.vintud.entity.User;
import com.thp.project.vintud.repository.UserRepository;

@RestController
@RequestMapping("api/public")
@CrossOrigin
public class PublicRestApiController {
    private UserRepository userRepository;

    public PublicRestApiController(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @GetMapping("test")
    public String test(){
        return "API Test";
    }

    
    @GetMapping("admin/users")
    public List<User> users(){
        return this.userRepository.findAll();
    }

}

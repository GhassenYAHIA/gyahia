package com.thp.project.vintud.security;

public class JwtProperties {
	public static final String SECRET = "projectvintud2020";
	public static final int EXPIRATION_TIME = 86400000; // 1 jour en millisecondes
	public static final String TOKEN_PREFIX = "BEARER ";
	public static final String HEADER_STRING = "Authorization";
}

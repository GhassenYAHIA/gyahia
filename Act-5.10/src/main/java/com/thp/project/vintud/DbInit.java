package com.thp.project.vintud;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.thp.project.vintud.entity.Role;
import com.thp.project.vintud.entity.Roles;
import com.thp.project.vintud.entity.User;
import com.thp.project.vintud.repository.UserRepository;

@Service
public class DbInit implements CommandLineRunner {
	@Autowired
	UserRepository userRepository;
	@Autowired
	PasswordEncoder passwordEncoder;

	@Override
	public void run(String... args) {
		userRepository.deleteAll();
		
		User ghassen = new User();
		ghassen.setUsername("ghassen");
		ghassen.setPassword(passwordEncoder.encode("g1988"));
		ghassen.setRole(new Role(Roles.USER));

		User admin = new User();
		admin.setUsername("admin");
		admin.setPassword(passwordEncoder.encode("admin"));
		admin.setRole(new Role(Roles.ADMIN));

		List<User> users = Arrays.asList(ghassen, admin);

		userRepository.saveAll(users);
	}

}

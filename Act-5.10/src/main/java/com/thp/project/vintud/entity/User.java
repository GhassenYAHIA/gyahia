package com.thp.project.vintud.entity;

import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int idUser;
	
	private String firstName;
	
	private String lastName;
	
	private String username;
	
	private String password;
	
	private String mail;
	
	private int phone;
	
	private String address;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "role_id")
	private Role role;
	
	@OneToMany (mappedBy = "owner"	, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Ad> postedAds;
	
	public List<Ad> getPostedAds() {
		return postedAds;
	}

	public void setPostedAds(List<Ad> postedAds) {
		this.postedAds = postedAds;
	}

	public List<Favorite> getFavoriteAds() {
		return favoriteAds;
	}

	public void setFavoriteAds(List<Favorite> favoriteAds) {
		this.favoriteAds = favoriteAds;
	}

	@OneToMany (mappedBy = "favouriteUser", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Favorite> favoriteAds;

	public User() {
	}

	public User(String firstName, String lastName, String username, String password, String mail, int phone,
			String address, Role role) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.password = password;
		this.mail = mail;
		this.phone = phone;
		this.address = address;
		this.role = role;
	}

	// getters
	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getMail() {
		return mail;
	}

	public int getPhone() {
		return phone;
	}

	public String getAddress() {
		return address;
	}

	public int getIdUser() {
		return idUser;
	}

	// setters

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idUser;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (idUser != other.idUser)
			return false;
		return true;
	}
}

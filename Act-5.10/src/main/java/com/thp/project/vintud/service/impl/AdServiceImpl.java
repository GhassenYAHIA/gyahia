package com.thp.project.vintud.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.thp.project.vintud.dto.AdDTO;
import com.thp.project.vintud.entity.Ad;


import com.thp.project.vintud.entity.User;


import com.thp.project.vintud.helper.ModelMapperConverter;
import com.thp.project.vintud.repository.AdRepository;
import com.thp.project.vintud.repository.UserRepository;
import com.thp.project.vintud.service.AdService;

@Transactional(propagation = Propagation.REQUIRES_NEW)
@Service
public class AdServiceImpl implements AdService {

	@Autowired
	AdRepository adRepository;

	UserRepository userRepository;
	
	@Override
	public List<AdDTO> findAllAds() {
		return ModelMapperConverter.convertAllAdToDTO(adRepository.findAll());
		
	}

	@Override
	public List<AdDTO> findAvailableAdsByuser(int userId) {
		return ModelMapperConverter.convertAllAdToDTO(adRepository.findByOwnerIdUserIsNot(userId));
	}

	@Override

	public List<AdDTO> findByNameAndCateg(String title, String category) {
		return ModelMapperConverter.convertAllAdToDTO(adRepository.findByTitleAdAndCategoryAdNameCategory(title, category));
	}

	@Override
	public List<AdDTO> findByName(String title) {
		return ModelMapperConverter.convertAllAdToDTO(adRepository.findByTitleAd(title));
	}

	@Override
	public List<AdDTO> findByCateg(String category) {
	
		return ModelMapperConverter.convertAllAdToDTO(adRepository.findByCategoryAdNameCategory(category));

	}



	}

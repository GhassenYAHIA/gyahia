package com.thp.project.vintud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.thp.project.vintud.dto.AdDTO;
import com.thp.project.vintud.dto.ConnexionDTO;
import com.thp.project.vintud.dto.UserDTO;
import com.thp.project.vintud.entity.Favorite;
import com.thp.project.vintud.repository.UserRepository;
import com.thp.project.vintud.service.UserService;

@RestController()
@RequestMapping(path = "/users")
public class UserController {

	@Autowired
	UserService usrservice;
	@Autowired
	UserRepository usrrepo;

	@PostMapping(value = "/{iduser}/ads")
	public UserDTO postAd(@PathVariable int iduser, @RequestBody AdDTO ad) {
		return usrservice.postAd(iduser, ad);
	}

	@PutMapping(value = "/{idUser}/ads")
	public UserDTO updateAd(@PathVariable int idUser, @RequestBody AdDTO ad) {
		return usrservice.updateAd(idUser, ad);
	}

	@RequestMapping(value = "/signin", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public UserDTO signin(@RequestBody UserDTO userDto) {
		return usrservice.createUser(userDto);
	}

}

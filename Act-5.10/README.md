**Pré-requis :**
Un environnement d'exécution Java 8 (Java Runtime Environment) doit être installé et configuré dans la variable système PATH.

Un OS Windows ou Linux


**Description** :
cette application restful ayant comme retour sous format Json, permet de gerer un site de vente en ligne:
- creer un utilisateur
- creer une annonce
- recuperer les annonces disponible par l'utilisateur
- attribuer un role à un utilisateur pour limiter ses actions
- filtrer et trier les recherche

**Inintialisation :**
- creer une base de données avec mysql nommée vintudSecurity
- port du serveur 8080


**Actions et Champs :**

POST localhost:XXXX/login : permet de se connecter avec un body intégrant un username et un password

GET localhost:XXXX/api/public/test authentification nécessaire

GET localhost:XXXX/api/public/admin/users  Récupération de la liste des utilisateurs.

						Authentification en tant qu'ADMINISTRATEUR (ADMIN) nécessaire.



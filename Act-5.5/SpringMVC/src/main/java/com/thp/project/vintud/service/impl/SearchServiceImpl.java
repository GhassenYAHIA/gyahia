package com.thp.project.vintud.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thp.project.vintud.entity.Search;
import com.thp.project.vintud.repository.SearchRepository;
import com.thp.project.vintud.service.SearchService;

@Service
public class SearchServiceImpl implements SearchService {

	@Autowired
	private SearchRepository repo;

	public void createSearch(Search s) {
		
	repo.save(s);
		
	}
}

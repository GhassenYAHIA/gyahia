package com.thp.project.vintud.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.thp.project.vintud.repository.UserRepository;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.thp.project.vintud", entityManagerFactoryRef="emf")

@ComponentScan(basePackages= {"com.thp.project.vintud"})

public class ApplicationConfiguration {

   @Bean
   public LocalEntityManagerFactoryBean emf() {
      LocalEntityManagerFactoryBean factoryBean = new LocalEntityManagerFactoryBean();
      factoryBean.setPersistenceUnitName("persistence");
      return factoryBean;
   }

   @Bean
   public JpaTransactionManager transactionManager() {
      JpaTransactionManager transactionManager = new JpaTransactionManager();
      transactionManager.setEntityManagerFactory(emf().getObject());
      return transactionManager;
   }
}

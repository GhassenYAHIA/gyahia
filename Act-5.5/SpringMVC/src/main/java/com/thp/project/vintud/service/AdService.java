package com.thp.project.vintud.service;

import org.springframework.stereotype.Repository;

import com.thp.project.vintud.entity.Ad;
import com.thp.project.vintud.entity.User;

@Repository
public interface AdService {

	void createAd(Ad ad);

	void viewAds();

	void getAdsByUser(User user);

	void getAdsByUserId(int id);

}

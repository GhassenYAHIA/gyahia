package com.thp.project.vintud.service;

import org.springframework.stereotype.Repository;

import com.thp.project.vintud.entity.Role;

@Repository
public interface RoleService {

	void createRole(Role role);
}

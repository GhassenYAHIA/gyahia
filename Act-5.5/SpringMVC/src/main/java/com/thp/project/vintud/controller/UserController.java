package com.thp.project.vintud.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller

public class UserController  {

	private static final long serialVersionUID = 1L;

	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public String index(ModelMap model) {
		model.addAttribute("message", "Liste of users");
		return "user";
	}
	
	
	

}
package com.thp.project.vintud.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.thp.project.vintud.entity.Role;
import com.thp.project.vintud.entity.User;

@Repository
@Transactional
public interface UserService {
	
	void createUser (User user);
	
	boolean login(String mail, String password);
	
	void updateUser(User user,String mail, String pseudo);
	
	void updateRole(User user, Role role);
	

}

package com.thp.project.vintud.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int idUser;
	@Column(name = "firstName")
	private String firstName;
	@Column(name = "LastName")
	private String lastName;
	@Column(name = "pseudo")
	private String pseudo;
	@Column(name = "u_password")
	private String password;
	@Column(name = "mail")
	private String mail;
	@Column(name = "phone")
	private int phone;
	@Column(name = "address")
	private String address;
	@ManyToOne
	@JoinColumn(name = "role_id")
	private Role role;
	
	@OneToMany (mappedBy = "userAd")
	private List<Ad> postedAd;
	
	@OneToMany (mappedBy = "favouriteUser")
	private List<Favorite> favoriteAd;

	public User() {
	}

	public User(String firstName, String lastName, String pseudo, String password, String mail, int phone,
			String address, Role role) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.pseudo = pseudo;
		this.password = password;
		this.mail = mail;
		this.phone = phone;
		this.address = address;
		this.role = role;
	}

	// getters
	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPseudo() {
		return pseudo;
	}

	public String getPassword() {
		return password;
	}

	public String getMail() {
		return mail;
	}

	public int getPhone() {
		return phone;
	}

	public String getAddress() {
		return address;
	}

	public int getIdUser() {
		return idUser;
	}

	// setters

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	
	public String toString() {
		return "User [idUser=" + idUser + ", firstName=" + firstName + ", lastName=" + lastName + ", pseudo=" + pseudo
				+ ", mail=" + mail + ", role=" + role + "]";
	}

}

package com.vintud.dao;

import java.sql.SQLException;

import com.vintud.entity.Search;

public interface ISearchDao{
	public int create(Search search) throws SQLException;
	public int delete(int id) throws SQLException;
	public String launch(int id) throws SQLException;
}

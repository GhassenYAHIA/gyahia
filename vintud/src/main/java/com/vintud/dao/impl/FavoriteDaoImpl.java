package com.vintud.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.vintud.dao.IFavoriteDao;
import com.vintud.dao.factory.FactoryDao;
import com.vintud.entity.Favorite;

import com.vintud.entity.Ad;
import com.vintud.entity.User;

public class FavoriteDaoImpl implements IFavoriteDao {

	private FactoryDao factoryDao;

	public FavoriteDaoImpl(FactoryDao factoryDao) {
		this.factoryDao = factoryDao;
	}

	@Override
	public int insertFavorite(Favorite fav, int userID, int announceId) throws SQLException {
		Connection conn = FactoryDao.getConnection();
		Statement stmt = null;
		ResultSet keys = null;
		int lastKey = 1;

		String query = "INSERT INTO favorite (favorite_date, ad_id,user_id) " + "VALUES ('" + fav.getFavouriteDate()
				+ "'," + announceId + "," + userID + ")";
		stmt = conn.createStatement();
		System.out.println(query);
		stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
		System.out.println("create ok");
		keys = stmt.getGeneratedKeys();

		while (keys.next()) {
			lastKey = keys.getInt(1);
		}
		User user = new User();
		user.setIdUser(userID);

		Ad ad = new Ad();
		ad.setIdAd(announceId);
		fav.setIdFavourite(lastKey);
		fav.setFavouriteUser(user);
		fav.setFavouriteAd(ad);
		System.out.println(fav);

		return lastKey;
	}

	public int delete(int favoriteId) throws SQLException {
		Connection conn = FactoryDao.getConnection();
		String query = "DELETE FROM favorite WHERE id =" + favoriteId;
		return conn.createStatement().executeUpdate(query);

	}

	@Override
	public List<Favorite> findAnnounce(int idAnnounce) throws SQLException {
		Connection conn = FactoryDao.getConnection();

		List<Favorite> favoris = new ArrayList<>();

		if (conn == null) {
			System.out.println("Error of connection to the databse. Please check if the server is running.");
		}
		String query = "SELECT * FROM favorite WHERE favorite.ad_id =" + idAnnounce + " ;";
		System.out.println(query);
		Statement stm = conn.createStatement();
		ResultSet rs = stm.executeQuery(query);

		while (rs.next()) {
			Favorite fav = new Favorite();
			fav.setIdFavourite(rs.getInt("id"));
			fav.setFavouriteDate(rs.getTimestamp("favorite_date"));
			User user = new User();
			user.setIdUser(rs.getInt("user_id"));
			fav.setFavouriteUser(user);
			Ad ad = new Ad();
			ad.setIdAd(rs.getInt("ad_id"));
			fav.setFavouriteAd(ad);

			favoris.add(fav);

		}

		return favoris;

	}

	@Override
	public int update(Favorite fav) throws SQLException {
		Connection conn = FactoryDao.getConnection();

		if (conn == null) {
			System.out.println("Error of connection to the databse. Please check if the server is running.");
		}

		String query = "Update favorite  SET favorite_date ='" + fav.getFavouriteDate() + "',ad_id ="
				+ fav.getFavouriteAd().getIdAd() + ", user_id=" + fav.getFavouriteUser().getIdUser() + " WHERE id = "
				+ fav.getIdFavourite() + ";";
		return conn.createStatement().executeUpdate(query);

	}

}

package com.vintud.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="favorite")
public class Favorite {
	@Id
	@GeneratedValue
	@Column(name="id")
	private int idFavourite;
	@Column(name="favorite_date")
	private Timestamp favouriteDate;
	@Column(name="ad_id")
	private Ad favouriteAd;
	@Column(name="user_id")
	private User favouriteUser;
	

	public Favorite(Timestamp favouriteDate) {

		this.favouriteDate = favouriteDate;
		
	}

	public Favorite() {
	}

	// getter
	public Timestamp getFavouriteDate() {
		return favouriteDate;
	}

	public User getFavouriteUser() {
		return favouriteUser;
	}

	public Ad getFavouriteAd() {
		return favouriteAd;
	}

	public int getIdFavourite() {
		return idFavourite;
	}

	// setter
	public void setFavouriteDate(Timestamp favouriteDate) {
		this.favouriteDate = favouriteDate;
	}

	public void setFavouriteUser(User favouriteUser) {
		this.favouriteUser = favouriteUser;
	}

	public void setFavouriteAd(Ad favouriteAd) {
		this.favouriteAd = favouriteAd;
	}

	public void setIdFavourite(int idFavourite) {
		this.idFavourite = idFavourite;
	}
	
	//cette methode permet d'afficher
	@Override
	public String toString() {
		return "Favorite [idFavourite=" + idFavourite + ", favouriteDate=" + favouriteDate + "]";
	}
	
	
	
}


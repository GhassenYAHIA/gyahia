package com.vintud.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "role")
public class Role {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private int idRole;
	@Column(name = "name")
	private static Roles nameRole;

	public Role() {
	}

	public Role(Roles nameRole) {
		Role.nameRole = nameRole;
	}

	public Roles getNameRole() {
		return nameRole;
	}

	public int getIdRole() {
		return idRole;
	}

	public void setNameRole(Roles nameRole) {
		Role.nameRole = nameRole;
	}

	public void setIdRole(int idRole) {
		this.idRole = idRole;
	}

}

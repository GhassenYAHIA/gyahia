import java.util.Scanner;

public class compterButin {

	public static void main(String[] args) {
		float nb1, nb2;
		float resultatInterm = 0;
		float resultatTotal = 0;
		String operationVoulue;
		String reponse;
		Scanner input = new Scanner(System.in);

		do {
			System.out.println("Veuillez saisir un nombre:");
			nb1 = input.nextInt();
			System.out.println("Veuillez saisir un deuxième nombre:");
			nb2 = input.nextInt();

			System.out.println("Saisissez vote mode de calcul:");
			operationVoulue = input.next();


			// selon cas 
			switch (operationVoulue) {
			case "+":
				resultatInterm = nb1 + nb2;
				break;

			case "-":
				resultatInterm = nb1 - nb2;
				break;

			case "/":
				resultatInterm = nb1 / nb2;
				break;

			case "*":
				resultatInterm = nb1 * nb2;
				break;
			}


			// ajouter le nouveau résultat à l'ancienne opération 
			
			resultatTotal += resultatInterm;

			System.out.println("Résultat total : " + resultatTotal
					+ ". Continuez ? (tapez \"Oui\" pour continuer et \"Non\" pour Sortir)");

			reponse = input.next();

		} while (reponse.equalsIgnoreCase("Oui"));

		System.out.println("Merci, à bientôt l'ami !");
	}

}

import java.util.Arrays;
import java.util.Scanner;
import java.lang.*;

public class orientationEtoile {
	public static void main(String[] args) {

		String[] prenoms = { "Alban", "Jim", "Bob", "Albatroz", "Jinto", "Bow", "Hijo", "Manu", "Seb", "Teilo",
				"Charles", "Xavier" };
		triTableau(prenoms);
		System.out.println(Arrays.toString(prenoms));

		String saisie;
		String recherche;
		do {
			Scanner input = new Scanner(System.in);
			System.out.println("Veuillez saisir votre nom :");
			saisie = input.nextLine();
			//input.close();
		} while (saisie.length() <= 0);

		String[] newTab = new String[prenoms.length+1];
		insertTableau(prenoms,saisie,newTab);
		System.out.println(Arrays.toString(newTab));

		do {
			Scanner input2 = new Scanner(System.in);
			System.out.println("Veuillez saisir le nom à chercher :");
			recherche = input2.nextLine();
			input2.close();
		} while (recherche.length() <= 0);

		rechercheDicho(newTab,recherche);

	}

	// methode de recherche dichotomique d'un mot dans un tableau
	public static void rechercheDicho(String[] tableau, String mot) {
		int i = 0;
		int j = tableau.length;
		boolean msg = false;
		int res;
		int pos; int k=0;

		while (i <= j && msg == false) {
			k = (i + j) / 2;
			res = mot.compareTo(tableau[k]);
			if (res == 0) {
				msg = true;
			} else {
				if (res < 0) {
					j = k - 1;
				} else {
					i = k + 1;
				}

			}
		}
		if (msg == true) {
			pos = k+1;
		} else {
			pos = -1;
		}
		;
		System.out.println("Votre entree " + mot + " se trouve à  la case " + pos);
	}

	// methode de tri de tableau dans l'ordre alphabetique

	static String[] triTableau(String[] tableau) {
		int k;
		boolean msg;
		String temp;
		for (int i = 0; i < tableau.length - 2; i++) {
			for (int j = tableau.length - 1; j > i; j--) {
				k = 0;
				msg = false;
				while (k < tableau[j].length() && k < tableau[j - 1].length() && msg == false) {
					if (tableau[j].charAt(k) < tableau[j - 1].charAt(k)) {
						temp = tableau[j - 1];
						tableau[j - 1] = tableau[j];
						tableau[j] = temp;
						msg = true;
					} else {
						if (tableau[j].charAt(k) == tableau[j - 1].charAt(k)) {
							k++;
						} else {
							msg = true;
						}
					}
				}

			}
		}
		return tableau;

	}

	// methode insertion d'un mot dans un tableau

	public static String[] insertTableau(String tableau[], String mot, String tableau2[]) {
	int i=0;
	int j;
	
	while(tableau[i].compareTo(mot)<0){
			tableau2[i]=tableau[i];
			i++;
        }
        tableau2[i]=mot;
        for (j=i+1;j<tableau2.length ;j++ ) {
        	tableau2[j]=tableau[j-1];
        }
    return tableau2;
    }
}

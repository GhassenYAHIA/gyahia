import java.util.Arrays;

public class ageApprentis{

	public static void main(String[] args){

		int[] ageArray = {45, 54, 23, 32, 64, 46, 28, 82};
		for(int i=0;i<ageArray.length-1;i++){
			int indice=maxTableau(ageArray,i,ageArray.length);
			int temp=ageArray[i];
			ageArray[i]=ageArray[indice];
			ageArray[indice]=temp;
		}
		System.out.println(Arrays.toString(ageArray));



	}
	static int maxTableau(int[] t,int min,int max){
		int interm=min;
		for (int i=min+1;i<max;i++){
			if (t[i]>t[interm]){
				interm=i;
			}
		}
		return interm;
	}
}



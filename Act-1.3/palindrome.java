import java.util.Scanner;

public class palindrome{
	public static void main(String[] args){

		String saisie;
		do {Scanner input = new Scanner(System.in);
			System.out.println("Veuillez saisir un mot :");
			saisie = input.nextLine();
			input.close();
			}
			while(saisie.length()<=0);

		if (comparerMot(saisie)==true){
			System.out.println("Votre mot : "+ saisie +" est un palindrome.");
		} 
			else {System.out.println("Votre mot : "+ saisie +" n'est pas un palindrome.");};

		
	}
	public static boolean comparerMot(String mot){
		int milieu=mot.length()/2;
		int l=mot.length()-1;
		int i=0;
		boolean msg=true;
		while(i<=milieu && msg==true){
			msg=false;
			if(mot.charAt(i)==mot.charAt(l-i)){
			msg=true;
			i++;
			}
		}
	return(msg);
	}
}



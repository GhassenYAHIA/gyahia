import java.util.Scanner;

public class UsingException {

	public static void main(String[] args) throws Exception{
		Scanner input = new Scanner(System.in);
		System.out.println("Saisir un nombre entre 10 et 30 inclus: ");
		int nb = input.nextInt();
		if(nb< 10 || (nb>30)){
			throw new Exception("The value is not in the allowed interval");
		}
		System.out.println("L'entier saisi est pris en compte : "+nb);
	}
}

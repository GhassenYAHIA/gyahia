import java.util.*;
import java.io.*;

public class SeLit {

	static void lecture(Scanner source) {
		while (source.hasNextLine()) {
			String s = source.nextLine();
			System.out.println("\"LU:\"" + s);
		}
	}

	static void lectureEnvers(Scanner source) {
		Stack<String> monStack=new Stack<>();

		while (source.hasNextLine()) {
			String s = source.nextLine();
			monStack.push(s);
			
		}
		while(!monStack.isEmpty()){
			  System.out.println(monStack.pop());
		}
	}

	/**public static void ecritureLigne(Scanner source) throws IOException{
		FileWriter monFichier=new FileWriter("monFichier.txt");
		Stack<String> monStack=new Stack<>();

		while (source.hasNextLine()) {
			String s = source.nextLine();
			monFichier.write(s);
			
		}
	}*/
	
	static public void main(String[] args) throws Exception {

		System.out.println("\n-------------------- Lecture du fichier à l'endroit --------------------\n");
		Scanner input = new Scanner(new FileReader("SeLit.java"));
		lecture(input);

		System.out.println("\n-------------------- Lecture du fichier à l'envers --------------------\n");
		Scanner input2 = new Scanner(new FileReader("SeLit.java"));
		lectureEnvers(input2);

		//Scanner input3 = new Scanner(new FileReader("SeLit.java"));
		//ecritureLigne(input3);
		
	}
}

/**
 * Ensuite, vous écrirez chaque ligne dans un fichier
 * différent (monFichier_L1.txt, monFichier_L2.txt, ...). Enfin, vous lirez
 * chacun des fichiers que vous avez créé en les listant grâce à la méthode vu
 * dans l'exercice précédent, puis vous afficherez le nombre de caractères
 * présents dans chacun des fichiers.
 */
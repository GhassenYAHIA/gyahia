
import java.io.*;

public class MaximumWithoutExceptions {
	public static void main(String args[]) throws IOException {
		try {
			BufferedReader br = new BufferedReader(new FileReader("data.txt"));

			try {
				int max = -1;
				String line = br.readLine();

				try {
					while (line != null) {
						int n = Integer.parseInt(line);

						if (n > max)
							max = n;
						try {
							line = br.readLine();
						} catch (IOException e) {
							System.out.println(e.getMessage());
						}

					}
				} catch (NumberFormatException e) {
					System.out.println(e.getMessage());
				}
				System.out.println("Maximum = " + max);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}
}

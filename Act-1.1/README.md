Description du contenu du dossier Act-1.1

Ce dossier comporte les fichiers d'extension txt en réponses aux activités du Module 1.1.

- qcm-1.1.txt : réponses au QCM
- canons.txt: calcul du nombres de tirs maximums avant que le navire coule
- grandNb.txt : détermination du plus grand nombre dans une liste de 20 nombres donnés
- Viser.txt : détermination du résultat du tir effectué (faire couler le beateau ennemi ou pas)
- Complexite.txt : résume les complexités des diffférents algorithmes dans ce dossier

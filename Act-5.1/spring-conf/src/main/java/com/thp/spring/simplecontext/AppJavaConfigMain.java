package com.thp.spring.simplecontext;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppJavaConfigMain {

	public static void main(String[] args) {
		/* Définition du chemin vers le context xml et son chargement */
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppMoussaillonConfig.class);

		Moussaillon m =  context.getBean("moussaillon",Moussaillon.class);

		System.out.println("Le prénom du moussaillon : " + m.getFirstName() + ".\nLe nom du moussaillon: "
				+ m.getLastName() + ".\nConfiguration utilisée: " + m.getConfig()
				+ ".\nBravo, vous venez de créer votre premier contexte Spring en " + m.getConfig() + "!");
		System.out.println("\n**********************\n");
		m.setFirstName("Ghassen");
		m.setLastName("Yahia");
		m.setConfig("Talan");
		System.out.println("Le prénom du moussaillon : " + m.getFirstName() + ".\nLe nom du moussaillon: "
				+ m.getLastName() + ".\nConfiguration utilisée: " + m.getConfig()
				+ ".\nBravo, vous venez de créer votre premier contexte Spring en " + m.getConfig() + "!");

	}
}

package com.thp.spring.simplecontext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppXMLMain {
	/* Définition du chemin vers le context xml et son chargement */
	@Autowired
	static ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

	public static void main(String[] args) throws Exception {

		/* chargement du bean moussaillon */
		Moussaillon m = (Moussaillon) context.getBean("moussaillon");

		/* Vérification s'il y a un bean pour cet id */
		//System.out.println(context.containsBean("moussaillon"));

		System.out.println("Le prénom du moussaillon : " + m.getFirstName() + ".\nLe nom du moussaillon: "
				+ m.getLastName() + ".\nConfiguration utilisée: " + m.getConfig()
				+ ".\nBravo, vous venez de créer votre premier contexte Spring en XML!");

	}
}

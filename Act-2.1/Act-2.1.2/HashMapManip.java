import java.util.HashMap;
import java.util.Map;

public class HashMapManip {
	public static void main(String[] args) {
		Map numberMapping = new HashMap<>();
		numberMapping.put("One", 1);
		numberMapping.put("Two", 2);
		numberMapping.put("Three", 3);
		numberMapping.put("Four", 4);

		System.out.println(numberMapping);

		Map personneVille = new HashMap<>();
		personneVille.put("Steve", "London");
		personneVille.put("John", "New York");
		personneVille.put("Rajeev", "Bungaluru");

		System.out.println(personneVille);

		System.out.println("*******************");
		// Vérifier s'il est vide | isEmpty()
		if (personneVille.isEmpty()) {
			System.out.println("Il est vide.");
		} else {
			System.out.println("Il n'est pas vide.");
		}
		System.out.println("*******************");
		// Trouver taille
		System.out.println("Taille = " + personneVille.size());
		System.out.println("*******************");
		// Vérifier si une clé existe ou pas
		if (personneVille.containsKey("John")) {
			System.out.println("Vrai");
		}
		if (personneVille.containsKey("Tom")) {
			System.out.println("Vrai");
		} else {
			System.out.println("Faux");
		}
		System.out.println("*******************");
		// Vérifier si une valeur existe ou pas
		if (personneVille.containsValue("Tunisie")) {
			System.out.println("Vrai");
		} else {
			System.out.println("Faux");
		}
		System.out.println("*******************");
		// Récupérer la valeur à l'aide d'une clé
		Object valeur = personneVille.get("Rajeev");
		System.out.println(valeur);

		System.out.println("*******************");
		// Modifier la valeur à l'aide d'une clé
		personneVille.put("Rajeev", "New Delhi");
		System.out.println(personneVille);
		
		System.out.println("*******************");
		//Supprimer une clé et sa valeur
		personneVille.remove("John");
		System.out.println(personneVille);
		
		System.out.println("*******************");
		//Supprimer une clé et sa valeur seulement si la clé et la valeur sont associées
		personneVille.remove("Steve","London");
		System.out.println(personneVille);
	}

}

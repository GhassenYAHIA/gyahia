Contenu du dossier Act-2.1

Trois dossiers :

1 - Act-2.1.1: Manipulation ArrayList (initialisation, add, remove, indexOf, size, get, contains, sort)

2 - Act-2.1.2: Manipulation HashMap (initialisation, put, isEmpty, size, containsKey, containsValue, get,remove)

3 - Act-2.1.3: Act-1.6.1 avec modification des tableaux en ArrayList. 
			  Ajout d'une classe utilisateur.
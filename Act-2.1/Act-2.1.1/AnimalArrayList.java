import java.util.Collections;
import java.util.ArrayList;
import java.util.Arrays;

public class AnimalArrayList {

    public static void main(String[] args) {
	ArrayList<String> animal = new ArrayList<>(Arrays.asList("Lion", "Tiger", "Cat", "Dog"));
	
	System.out.println(animal+"\n**********");

	animal.add(2,"Elephant");
	System.out.println(animal+"\n**********");
	
	animal.addAll(animal);
	
	System.out.println(animal);
	System.out.println("Taille du tableau :"+animal.size()+"\n**********");
	
	supprIndex(4,animal);
	supprNom("Tiger" , animal);
	rechercheIndex(6,animal);
	rechercheNom("Elephant",animal);
	System.out.println("Tri avec compareTo et swap \n"+triCustomComparateur(animal));
	System.out.println("Tri par Collections.sort \n"+triCollectionSort(animal));
	System.out.println("Tri par ArrayList.sort \n"+triArrayListSort(animal));
	


	}
    //suppression d'un élément par son index
    static void supprIndex(int index ,ArrayList<String> liste) {
    System.out.println("Suppression de l'élément en position : " + index);
	System.out.println("Suppression de l'élément :"+liste.get(index-1));	
	liste.remove(index);
	System.out.println("Nouveau tableau : "+liste);
	System.out.println("Taille du tableau :"+liste.size()+"\n**********");
    }


    //suppression d'en élément par son nom
    static void supprNom(String nom, ArrayList<String> liste) {
	int index = liste.indexOf(nom);

	while(index != -1)
	{	liste.remove(nom);
		index = liste.indexOf(nom);	  
    }
     	System.out.println("Suppression de l'élément :" + nom);
	    System.out.println("Nouveau tableau : "+liste);
	    System.out.println("Taille du tableau :"+liste.size()+"\n**********");
	
    }

    //recherche d'un élément par son index
    public static void rechercheIndex(int index ,ArrayList<String> liste) {

		String mot = liste.get(index);
		System.out.println("L'élément d'index "+index+" est :"+mot+".\n**********");

	}
	//recherche d'un élément par son nom
	public static void rechercheNom(String nom,ArrayList<String> liste) {

		if (liste.contains(nom)) {
			System.out.println("L'élément \"" + nom + "\" existe à la postion N°" + liste.indexOf(nom)+".\n**********");
		}
		else{System.out.println("L'élément \"" + nom + "\" n'existe pas.\n***********");}
	}

	//tri avec Collection.sort
	public static ArrayList<String> triCollectionSort(ArrayList<String> liste) {
		Collections.sort(liste);
		return liste;
	}

	//tri avec ArrayList.sort
	public static ArrayList<String> triArrayListSort(ArrayList<String> liste) {
		liste.sort(null);
		return liste;
	}

	//tri avec un compareTo et swap
	public static ArrayList<String> triCustomComparateur(ArrayList<String> liste) {
		for (int i = 0; i < liste.size() - 1; i++) {
			for (int j = i + 1; j < liste.size(); j++) {
				if (liste.get(i).compareToIgnoreCase(liste.get(j)) > 0) {
					Collections.swap(liste, i, j);
				}
			}
		}
		return liste;
	}

}

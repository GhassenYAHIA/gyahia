import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class PlaylisteImpl implements Playliste {
	final int MAX_MUSIQUES = 100;
	private String nom;
	private String genre;
	private int nombreTitres;
	private ArrayList<MusiqueImpl> listeMusique = new ArrayList<>();
	private int id;
	static int nbPlaylistes = 0;

	public PlaylisteImpl(String nom, String genre, int nombreTitres) {
		this.nom = nom;
		this.genre = genre;
		this.nombreTitres = nombreTitres;
		this.id = ++nbPlaylistes;
	}

	// getters
	public String getNom() {
		return nom;
	}

	public String getGenre() {
		return genre;
	}

	public int getNombreTitres() {
		return nombreTitres;
	}

	public ArrayList<MusiqueImpl> getListeMusique() {
		return listeMusique;
	}

	// setters
	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public void setNombreTitres(int nombreTitres) {
		this.nombreTitres = nombreTitres;
	}

	public void setListeMusique(ArrayList<MusiqueImpl> listeMusique) {
		this.listeMusique = listeMusique;
	}

	// methodes de l'interface

	// affichage playliste et ses caractéristiques

	public String toString() {
		String chaine = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nLa playlist n°" + this.id + " s\'appelle " + this.nom
				+ ".\nElle contient " + this.nombreTitres + " chansons de genre " + this.genre
				+ ".\nElle contient les titres suivants: \n";
		for (int i = 0; i < this.nombreTitres; i++) {
			chaine = chaine + this.listeMusique.get(i) + "\n\n";
		}
		;
		return chaine;
	}

	// ajout d'une chanson
	public void ajouterMusique(MusiqueImpl chanson) {
		this.listeMusique.add(chanson);
		this.nombreTitres++;
	}

	// suppression doublon

	public ArrayList<MusiqueImpl> supprimerDoublon() {
		for (int i = 0; i < this.nombreTitres; i++) {
			for (int j = i + 1; j < this.nombreTitres; j++) {
				if (this.listeMusique.get(i) == this.listeMusique.get(j)) {
					this.listeMusique.remove(j);
				}

				this.nombreTitres--;

			}
		}
		return this.listeMusique;
	}

	// affichage des titres communs à 2 playlistes
	public void afficherMemeMusique(PlaylisteImpl playlist) {

		for (int i = 0; i < this.nombreTitres; i++) {
			for (int j = 0; j < playlist.nombreTitres; j++) {
				if (this.listeMusique.get(i) == playlist.listeMusique.get(j)) {
					System.out.println("Ce titre :\n " + playlist.listeMusique.get(i)
							+ "\nest présent dans les deux playslistes.");
				}
			}
		}
	}

	// suppression chanson d'une playliste
	public void supprChanson(MusiqueImpl chanson) {
		this.listeMusique.remove(chanson);
	}

	public void triTitre() {
		Comparator<MusiqueImpl> comparerTitre = (MusiqueImpl m1, MusiqueImpl m2) -> m1.getTitre()
				.compareTo(m2.getTitre());
		Collections.sort(this.listeMusique, comparerTitre);
		System.out.println("\nLa playliste est triée par titre dans l'ordre croissant.");

	}

	public void triTitreDecr() {
		Comparator<MusiqueImpl> comparerTitre = (MusiqueImpl m1, MusiqueImpl m2) -> m1.getTitre()
				.compareTo(m2.getTitre());
		Collections.sort(this.listeMusique, comparerTitre.reversed());
		System.out.println("\nLa playliste est triée par titre dans l'ordre croissant.");

	}
}

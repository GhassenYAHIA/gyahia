import java.util.ArrayList;

public interface Playliste{
	public String toString();

	public void ajouterMusique(MusiqueImpl chanson);

	public ArrayList<MusiqueImpl> supprimerDoublon();
	
	public void afficherMemeMusique(PlaylisteImpl playliste);
}
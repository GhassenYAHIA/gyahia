import java.util.ArrayList;

public class PlayListeUI {
	public static void main(String[] args) {
		MusiqueImpl m1 = new MusiqueImpl("Ghassen", "Najeh", "Emna", "Rien");
		MusiqueImpl m2 = new MusiqueImpl("Ines", "Haithem", "Mohamed", "Encore rien");
		MusiqueImpl m3 = new MusiqueImpl("Ghassen", "Najeh", "Emna", "Rien");
		MusiqueImpl m4 = new MusiqueImpl("Ines", "Haithem", "Mohamed", "Encore rien");
		MusiqueImpl m5 = new MusiqueImpl("Senda", "Raoudha", "Ali", "Toujous rien");
		System.out.println(m1);
		System.out.println(m2);
		System.out.println(m3);

		m1.equals(m2);
		m1.equals(m3);

		PlaylisteImpl p1 = new PlaylisteImpl("Première", "Rock", 0);
		p1.ajouterMusique(m1);
		p1.ajouterMusique(m2);
		p1.ajouterMusique(m3);

		System.out.println(p1 + "\n");
		p1.supprimerDoublon();
		System.out.println(p1);

		PlaylisteImpl p2 = new PlaylisteImpl("Seconde", "Rock", 0);
		p2.ajouterMusique(m1);
		p2.ajouterMusique(m4);
		p2.ajouterMusique(m5);

		p1.afficherMemeMusique(p2);

		System.out.println("\n\n**********Tests sur la partie User**********");
		ArrayList<PlaylisteImpl> liste = new ArrayList<>();
		liste.add(p1);
		liste.add(p2);
		User u1 = new User("Banner", "Bruce", "Hulk", liste);
		System.out.println(u1);

		u1.rechercheAuteur("Raoudha");
		u1.rechercheAuteur("Ghassen");

		u1.rechercheTitre("Emna");
		u1.rechercheTitre("Senda");

		u1.getListe().get(0).supprChanson(m1);

		u1.getListe().get(0).triTitre(); //tri dans l'ordre croissant
		System.out.println(u1.getListe().get(0));
		
		u1.getListe().get(1).triTitreDecr();
		System.out.println(u1.getListe().get(1));
	}
}
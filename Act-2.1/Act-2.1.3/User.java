import java.util.ArrayList;
import java.util.Collections;

public class User {
	private String nom;
	private String prenom;
	private String pseudo;
	private ArrayList<PlaylisteImpl> liste = new ArrayList<>();

	public User(String nom, String prenom, String pseudo, ArrayList<PlaylisteImpl> liste) {
		this.nom = nom;
		this.prenom = prenom;
		this.pseudo = pseudo;
		this.liste = liste;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public ArrayList<PlaylisteImpl> getListe() {
		return liste;
	}

	public void setListe(ArrayList<PlaylisteImpl> liste) {
		this.liste = liste;
	}

	//toString
	public String toString() {
		return "Cet utilisateur s'appelle : " + this.prenom + " " + this.nom.toUpperCase()
				+ ", utilisant le pseudo :" + this.pseudo + ".\nSes playlistes contiennent :\n\n"+this.liste;
	}


	/** */
   
  
    //recherche musique par auteur
    public void rechercheAuteur(String auteur) {
	boolean msg = false;
	
	for(PlaylisteImpl playlist : this.liste) {	    
	    for(MusiqueImpl musique : playlist.getListeMusique()) {
		
		    if (musique.getAuteur().equalsIgnoreCase(auteur)) {
			System.out.println("\nLes chanson(s) de l'auteur  " + auteur + " sont: \n " + musique + "\n\nNous les trouvons dans la playlist " + playlist.getNom()+".");
			msg = true;
		    }
		}
	}
	if(msg == false) {
	    System.out.println("\nCet auteur "+ auteur + " n'a aucune chanson dans les playlistes.");
	}
    }
    //recherche musique par titre

	public void rechercheTitre(String titre) {
	boolean msg = false;
	
	for(PlaylisteImpl playlist : this.liste) {	    
	    for(MusiqueImpl musique : playlist.getListeMusique()) {
		
		    if (musique.getTitre().equalsIgnoreCase(titre)) {
			System.out.println("\nLe titre \"" + titre + "\" existe :\n " + musique + "\n\nNous le trouvons dans la playlist " + playlist.getNom()+".");
			msg = true;
		    }
		}
	}
	if(msg==false) {
	    System.out.println("\nLe titre " + titre + " n'existe pas dans les playlistes.");
	}
    }

 }
	
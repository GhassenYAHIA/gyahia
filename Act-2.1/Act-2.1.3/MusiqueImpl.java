public class MusiqueImpl implements Musique {
	private String titre;
	private String auteur;
	private String interprete;
	private String genre;
	private int id;
	static int nb = 0;

	public MusiqueImpl(String titre, String auteur, String interprete, String genre) {
		this.titre = titre;
		this.auteur = auteur;
		this.interprete = interprete;
		this.genre = genre;
		this.id = ++nb;
	}

	// getters
	public String getTitre() {
		return titre;
	}

	public String getAuteur() {
		return auteur;
	}

	public String getInterprete() {
		return interprete;
	}

	public String getGenre() {
		return genre;
	}

	// setters
	public void setTitre(String titre) {
		this.titre = titre;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	public void setInterprete(String interprete) {
		this.interprete = interprete;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	// implementation des méthodes d'interface musique
	public String toString() {
		return "-----------------Chanson n°" + this.id + "-----------------\n"
				+ "Ce titre porte le nom \"" + this.titre + "\".\nAuteur: " 
				+ this.auteur + ".\nInterprète: "
				+ this.interprete + ".\nGenre: " + this.genre + ".";
	}

	public void equals(MusiqueImpl musique) {
		boolean msg = true;
		if (this == musique)
			if (musique == null)
				msg = false;
		if (getClass() != musique.getClass())
			msg = false;
		MusiqueImpl other = musique;
		if (titre != other.titre)
			msg = false;
		if (msg) {
			System.out.println("\nLa chanson " + this.id + " et la chanson " + musique.id + " sont les mêmes.");
		} else {
			System.out.println("\nLa chanson " + this.id + " et la chanson " + musique.id + " ne sont pas les mêmes.");
		}
	}

}
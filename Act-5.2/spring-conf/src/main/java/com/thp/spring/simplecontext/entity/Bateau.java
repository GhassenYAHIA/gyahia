package com.thp.spring.simplecontext.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "bateau")
public class Bateau {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "name")
	private String nom;
	@Column(name = "type")
	private String type;
	@Column(name = "size")
	private double taille;
	@OneToMany(mappedBy = "bateau")
	private List<Moussaillon> moussaillons;

	public Bateau() {

	}

	public Bateau(String nom, String type, double taille) {
		super();
		this.nom = nom;
		this.type = type;
		this.taille = taille;
	}

	public int getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public String getType() {
		return type;
	}

	public double getTaille() {
		return taille;
	}

	public List<Moussaillon> getMoussaillons() {
		return moussaillons;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setTaille(double taille) {
		this.taille = taille;
	}

	public void setMoussaillons(List<Moussaillon> moussaillons) {
		this.moussaillons = moussaillons;
	}

	@Override
	public String toString() {
		return "Bateau [id=" + id + ", nom=" + nom + ", type=" + type + ", taille=" + taille + ", moussaillons="
		/* + moussaillons + "]" */;
	}

}

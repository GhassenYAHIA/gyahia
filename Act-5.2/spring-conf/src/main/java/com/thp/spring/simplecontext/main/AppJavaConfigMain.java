package com.thp.spring.simplecontext.main;

import com.thp.spring.simplecontext.dao.impl.BateauDAOImpl;
import com.thp.spring.simplecontext.dao.impl.MoussaillonDAOImpl;
import com.thp.spring.simplecontext.entity.Bateau;
import com.thp.spring.simplecontext.entity.Moussaillon;

public class AppJavaConfigMain {

	public static void main(String[] args) {
		/*
		 * Bateau b1 = new Bateau("carthage", "barque", 10); BateauDAOImpl b = new
		 * BateauDAOImpl(); b.create(b1);
		 */
		Moussaillon m1 = new Moussaillon();

		m1.setFirstName("john");
		m1.setLastName("D");
		m1.setConfig("P");
		// m1.setBateau(b1););

		MoussaillonDAOImpl mdao = new MoussaillonDAOImpl();
		/*
		 * System.out.println("persist new moussaillon"); mdao.create(m1);
		 * System.out.println("persist finished");
		 */
		// mdao.create(m1);
		// mdao.update(m1, "Johnny", "Depp", "PirateCaraibes");
		// mdao.delete(m1);
		mdao.findById(19);
	}
}

package com.thp.spring.simplecontext.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.thp.spring.simplecontext.dao.MoussaillonDAO;
import com.thp.spring.simplecontext.entity.Moussaillon;

@Repository
public class MoussaillonDAOImpl implements MoussaillonDAO {
	@PersistenceContext
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistence");
	EntityManager em = emf.createEntityManager();

	@Override
	public int create(Moussaillon moussaillon) {
		System.out.println("Creating new entry for moussaillon:\n");
		em.getTransaction().begin();
		em.persist(moussaillon);
		em.getTransaction().commit();

		return 0;
	}

	@Override
	public Moussaillon update(Moussaillon moussaillon, String firstName, String lastName, String config) {
		System.out.println("Updating entry for moussaillon:\n");
		moussaillon.setFirstName(firstName);
		moussaillon.setLastName(lastName);
		moussaillon.setConfig(config);
		em.getTransaction().begin();
		em.merge(moussaillon);
		em.getTransaction().commit();
		return null;
	}

	@Override
	public void delete(Moussaillon moussaillon) {
		System.out.println("Deleting entry for moussaillon:\n");
		em.getTransaction().begin();
		em.remove(moussaillon);
		em.getTransaction().commit();

	}

	@Override
	public Moussaillon findById(int id) {
		em.getTransaction().begin();
		Moussaillon foundM = em.find(Moussaillon.class, id);
		System.out.println(foundM.toString());
		em.getTransaction().commit();
		return null;
	}

	@Override
	public List<Moussaillon> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Moussaillon> findAllByBateau(int bateauId) {
		// TODO Auto-generated method stub
		return null;
	}

}

package com.thp.spring.simplecontext.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.thp.spring.simplecontext.dao.BateauDAO;
import com.thp.spring.simplecontext.entity.Bateau;
import com.thp.spring.simplecontext.entity.Moussaillon;


@Repository
public class BateauDAOImpl implements BateauDAO{

	EntityManagerFactory emf=Persistence.createEntityManagerFactory("persistence");
	EntityManager em=emf.createEntityManager();
	
	@Override
	public int create(int id, String name, String type, double taille) {
	
		return 0;
	}

	@Override
	public int create(Bateau bateau) {
		em.getTransaction().begin();
		em.persist(bateau);
		em.getTransaction().commit();
		//em.flush();
		return 0;
	}

	@Override
	public Bateau update(Bateau bateau, String name, String type, double taille) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Bateau bateau) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Bateau findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Bateau findByMoussaillon(Moussaillon moussaillon) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Bateau> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

}

package com.thp.spring.simplecontext.dao;

import java.util.List;

import com.thp.spring.simplecontext.entity.Moussaillon;

public interface MoussaillonDAO {
	int create(Moussaillon moussaillon);

	Moussaillon update(Moussaillon moussaillon, String firstName, String lastName, String config);

	void delete(Moussaillon moussaillon);

	Moussaillon findById(int id);

	List<Moussaillon> findAll();

	List<Moussaillon> findAllByBateau(int bateauId);
}

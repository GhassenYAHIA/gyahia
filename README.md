Répertoire contenant des dossiers nommés Act-i.j
où i est le numéro du module et j est le numéro de la leçon.

Chaque dossier contient l'ensemble des réponses aux différentes activités de chaque leçon.

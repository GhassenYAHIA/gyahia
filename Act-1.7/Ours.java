public class Ours extends Predateur implements PredateurAction, OursAction {

	private String facteurAgressivite;
	private String puissance;
	private String statutHibernation;

	public Ours(Sexe sexe, Age age,  String force,Groupe groupe, String facteurAgressivite, String puissance,
			String statutHibernation) {
		super(sexe, age,  force,groupe);
		this.facteurAgressivite = facteurAgressivite;
		this.puissance = puissance;
		this.statutHibernation = statutHibernation;

	}
	
	// accesseurs et mutateurs propres à ours
	
	public String getfacteurAgressivite() {
		return facteurAgressivite;
	}

	public void setfacteurAgressivite(String facteurAgressivite) {
		this.facteurAgressivite = facteurAgressivite;
	}

	public String getPuissance() {
		return puissance;
	}

	public void setPuissance(String puissance) {
		this.puissance = puissance;
	}

	public String getStatutHibernation() {
		return statutHibernation;
	}

	public void setStatutHibernation(String statutHibernation) {
		this.statutHibernation = statutHibernation;
	}

	// implementations des methodes de l'interface predateur 
	
	public void seNourrir() {
		System.out.println("L'ours se nourrit de tout puisqu'il est omnivore.");
	}

	public void chasser() {
		System.out.println("L'ours chasse en solitaire.");
	}

	public void courrir() {
		System.out.println("Il a une vitesse  aux alentours de 40km/h");
	}

	public void seReproduire() {
		System.out.println("L'ours se reproduit ten saison de chaleur.");
	}

	public void emettreSon() {
		System.out.println("L'ours hurle.");
	}
	
	//methodes propres à Ours
	public void grimper() {
		System.out.print("L'ours grimpe des arbes.");
	}

	public void creuser() {
		System.out.println("Il creuse des trous pour se nourrir ou des cavernespour hiberner.");

	}

	public void hiberner() {
		System.out.println("Il hiberne en hiver pour une période moyenne de 150 jours.");

	}
	//affichage des caractéristiques de Ours communes auxx prédateurs et propres à lui
	
	public String toString() {
		return "Cet ours est " + this.getAge() + ", de sexe " + this.getSexe() + ", vit en " + this.getGroupe()
				+ ", a une force de " + this.getForce() + ",  un de facteur d'Agressivitéé " + this.facteurAgressivite
				+ ", une puissance " + this.puissance + " et son statut d'hibernation est " + this.statutHibernation
				+ ".\n";
	}

}

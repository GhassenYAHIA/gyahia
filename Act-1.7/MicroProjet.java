import java.util.Arrays;

public class MicroProjet {

	public static void main(String[] args) {
        
        Lion l1 = new Lion(Sexe.MALE, Age.VIEUX, "Puissant", Groupe.GROUPE, false, "Très Impétueux");
		Lion l2 = new Lion(Sexe.FEMELLE, Age.ADULTE, "Puissant", Groupe.GROUPE, true, "Très Impétueux");
        Ours o1 = new Ours(Sexe.MALE, Age.JEUNE, "fort",Groupe.SOLITAIRE,  "Très aggressif", "Ultra puissant", "en cours");
        //création d'un tableau de Predateur
		Predateur[] p = new Predateur[]{l1,o1,l2};

		//test sur entendre son en facteur de se réveiller et malade
		l1.seReveiller();
		l1.malade();
		l1.entendreSon();

		// test sur emettre son et se séparer en facteur partir ou retourner au groupe
		l1.partirGroupe();
		l1.seSeparer();
		l1.emettreSon();
		
		//test sur rang en fonction de la domination
		l1.setRang("Beta");
		System.out.println(l1.getRang());
        l1.setFacteurDomination(true);
		System.out.println(l1.getRang());

		
		System.out.println(Arrays.toString(p));
		System.out.println(p[1].getAge());
		System.out.println(p[0].getSexe());
		
		
	}

}

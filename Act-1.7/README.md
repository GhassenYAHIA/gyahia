Micro Projet Lion

Ce dossier comporte 8 fichiers:
1.  MicroProjet.java
2.  Predateur.java
3.  PredateurAction.java
4.  Lion.java
5.  LionAction.java
6.  Ours.java
7.  OursAction.java
8.  README.md

Ils sont catégorisés comme suit:

	1/ programme principal: MicroProjet
	2/ classe mère: Predateur
	3/ classes filles: Lion et Ours
	4/ interfaces: PredateurAction, Lion Action et OursAction
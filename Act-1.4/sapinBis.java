import java.util.Scanner;

public class sapinBis {
	public static void main(String[] args) {
		int n;
		Scanner input = new Scanner(System.in);
		System.out.println("Combien de triangles par sapin?");
		n = input.nextInt();
		dessinTriangle(n);

		for (int i = 0; i < n; i++) { //dessine la branche, pas toujour au milieu
			ligneVide(4 * n / 3 - 1);
			ligneSymb(n);
			System.out.println(" ");
		}

	}

	static void dessinTriangle(int n) {
		for (int i = n - 1; i < 2 * n - 1; i++) {
			for (int j = 0; j <= i; j++) {
				ligneVide(n - j + 1);
				ligneSymb(2 * j + 1);
				System.out.println(" ");
			}
		}
	}

	static void ligneVide(int n) {
		String resultat = " ";
		if (n > 0) {
			resultat.concat(resultat);
			ligneVide(n - 1);
		}
		System.out.print(resultat);
	}

	static void ligneSymb(int n) {
		String resultat = "*";
		if (n > 0) {
			System.out.print(resultat);
			resultat.concat(resultat);
			ligneSymb(n - 1);
		}
	}

}

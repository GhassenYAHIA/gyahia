export class Post {
  [x: string]: any;
  
  constructor(public title: string, public content: string, public loveIts: number, public created_at: Date) {
    
  }
}

import { Post } from './post';

export const POSTS: Post[] = [
    { title: "Post I", content: "Contenu du premier post.", loveIts: 2, created_at: new Date("May 04 2020 16:40") },
    { title: "Post II", content: "Contenu du deuxième post.", loveIts: -1, created_at: new Date("May 04 2020 16:42") },
    { title: "Post III", content: "Enfin le troisième post est là.", loveIts: 0, created_at: new Date("May 04 2020 16:44") }
];

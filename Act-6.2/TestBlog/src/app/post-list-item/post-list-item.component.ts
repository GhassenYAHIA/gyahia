import { Component, Input, OnInit } from '@angular/core';
import { PostService } from '../post.service';
import { Post } from '../post';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {
  @Input() post: Post;
 
  constructor(private postService: PostService) { }

  ngOnInit() {
  }

  onLike() {
    this.postService.onLike(this.post);
  }

  onDislike() {
    this.postService.onDislike(this.post);
  }

  onDelete() {
    this.postService.deletePost(this.post);
  }

  /*onEdit(i) {
    this.postService.editPost(this.post);
  }*/

}

import { Subject } from 'rxjs';
import { Post } from './post';
import { POSTS } from './mock-posts';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

export class PostService {

  postsSubject = new Subject<Post[]>()

  posts: Post[] = POSTS;


  postForm: FormGroup;
  formBuilder: FormBuilder;
  router: Router;


  emitPostSubject() {
    this.postsSubject.next(this.posts.slice());
  }

  onDislike(post: Post) {
    post.loveIts--;
  }

  onLike(post: Post) {
    post.loveIts++;
  }

  addPost(post: Post) {
    this.posts.push(post);
  }

  deletePost(post: Post) {
    this.posts.splice(this.posts.findIndex(obj => obj === post), 1);
    this.emitPostSubject()
  }





}
